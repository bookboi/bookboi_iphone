//
//  LoginViewController.m
//  BookBoi_iPhone
//
//  Created by Anan Mallik on 8/22/14.
//  Copyright (c) 2014 Mallik. All rights reserved.
//

#import "LoginViewController.h"

@interface LoginViewController ()
- (IBAction)meLogin:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *myPin;
@property (weak, nonatomic) IBOutlet UITextField *email;
- (IBAction)hideKeys:(id)sender;

@end

@implementation LoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.email.text = [[NSUserDefaults standardUserDefaults]
                  stringForKey:@"myEmail"];
    self.myPin.text = [[NSUserDefaults standardUserDefaults]
                       stringForKey:@"myPin"];
    
    //text field delegates set to self
    self.email.delegate = self;
    self.myPin.delegate = self;

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.myPin resignFirstResponder];
    [self Login];
    return YES;
}


-(void)Login
{
    
    if (self.email.text && self.email.text.length == 0)
    {
        
        UIAlertView * alert =[[UIAlertView alloc ] initWithTitle:@"Oops..."
                                                         message:@"Please Enter the email you registered with."
                                                        delegate:self
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles: nil];
        [alert show];
        
    }
    if (self.myPin.text && self.myPin.text.length != 4)
    {
        UIAlertView * alert =[[UIAlertView alloc ] initWithTitle:@"Oops..."
                                                         message:@"Please Enter the PIN you received in the email."
                                                        delegate:self
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles: nil];
        [alert show];
    }
    else if ([self.email.text rangeOfString:@".com"].location != NSNotFound||[self.email.text rangeOfString:@".edu"].location != NSNotFound||[self.email.text rangeOfString:@".org"].location != NSNotFound||[self.email.text rangeOfString:@".net"].location != NSNotFound||[self.email.text rangeOfString:@".us"].location != NSNotFound)
    {
        
        NSString *baseURL = @"http://52.24.55.43/b9ef165b255673dde47bff07f4390fb1/login.php?email=";
        
        baseURL = [baseURL stringByAppendingString:self.email.text];
        baseURL = [baseURL stringByAppendingString:@"&pin="];
        baseURL = [baseURL stringByAppendingString:self.myPin.text];
        
        NSURL *url = [NSURL URLWithString:baseURL];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        NSURLResponse *response;
        NSError *error;
        NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        NSLog(@"Response: %@", [[NSString alloc] initWithData:responseData encoding:NSASCIIStringEncoding] );
        
        
        NSString *login = [[NSString alloc] initWithData:responseData encoding:NSASCIIStringEncoding];
        
        if([login isEqualToString:@"YES"])
        {
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults setObject:self.email.text forKey:@"myEmail"];
            [defaults setObject:self.myPin.text forKey:@"myPin"];
            [defaults setBool:TRUE forKey:@"myLogin"];
            
            [self performSegueWithIdentifier:@"goHome" sender:self];
            
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Success!" message:@"Welcome to Skimp." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
        else
        {
            UIAlertView * alert =[[UIAlertView alloc ] initWithTitle:@"Oops..."
                                                             message:@"Incorrect Username or Password. Please try again!"
                                                            delegate:self
                                                   cancelButtonTitle:@"OK"
                                                   otherButtonTitles: nil];
            [alert show];
        }
    }
    else
    {
        UIAlertView * alert =[[UIAlertView alloc ] initWithTitle:@"Oops..."
                                                         message:@"Please enter a valid Email address :)"
                                                        delegate:self
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles: nil];
        [alert show];
    }

    
}



- (IBAction)meLogin:(id)sender
{
    
    [self Login];
    
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    //if screen size is smaller than iphone 5, move the view up when editing to allow user to see data entry
    if((double)[ [UIScreen mainScreen] bounds].size.height < 560)
        {
            [self.view setFrame:CGRectMake(0,-110,self.view.frame.size.width, self.view.frame.size.height)];
        }
    printf("field began editing\n");
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    //if view was moved up at beginning of editing, return to normal (as long as no other editing is occurring)
    
    if((double)[ [UIScreen mainScreen] bounds].size.height < 560 )
    {
        [self.view setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    }
    printf("field finished editing\n");
        
    //}
}



- (IBAction)hideKeys:(id)sender
{
    
    [self.myPin endEditing:YES];
    [self.email endEditing:YES];
    
}
@end
