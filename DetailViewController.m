//
//  DetailViewController.m
//  SKIMP
//
//  Created by Levi mallik on 28/05/2015.
//  Copyright (c) 2015 BookBoi Inc. All rights reserved.
//

#import "DetailViewController.h"

@interface DetailViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *pictureBox;
@property (weak, nonatomic) IBOutlet UILabel *textLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *emailLabel;


- (IBAction)sendEmail:(id)sender;


@end

@implementation DetailViewController

NSString *username;
NSString *ImageURL;
NSString *theMessage;
NSString *thePrice;
NSString *theUsername;
NSString *theSkimpID;
NSString *theTitle;
NSString *theDeviceID;
NSString *theEmail;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        
        sleep(0.1);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self setPicture];
            
        });
    });
    
    [self setNeedsStatusBarAppearanceUpdate];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}


-(void)viewWillAppear:(BOOL)animated
{
    self.pictureBox.image = [UIImage imageNamed:@"logo.png"];
    
    ImageURL = [[NSUserDefaults standardUserDefaults]
                stringForKey:@"imageURL"];
    
    theMessage = [[NSUserDefaults standardUserDefaults]
                  stringForKey:@"theMessage"];
    
    thePrice = [[NSUserDefaults standardUserDefaults]
                stringForKey:@"thePrice"];
    
    theUsername = [[NSUserDefaults standardUserDefaults]
                   stringForKey:@"theUsername"];
    
    theSkimpID = [[NSUserDefaults standardUserDefaults]
                  stringForKey:@"theSkimpID"];
    
    username = [[NSUserDefaults standardUserDefaults]
                stringForKey:@"myUsername"];
    
    theTitle = [[NSUserDefaults standardUserDefaults]
                stringForKey:@"theTitle"];
    
    theDeviceID = [[NSUserDefaults standardUserDefaults]
                stringForKey:@"deviceid"];
    
    theEmail = [[NSUserDefaults standardUserDefaults]
                   stringForKey:@"theEmail"];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        
        sleep(0.1);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self setPicture];
            
        });
    });
}

-(void)launchMailAppOnDevice
{
    NSString *recipients = @"mailto:";
    
    recipients = [recipients stringByAppendingString:[[NSUserDefaults standardUserDefaults]
                                                      stringForKey:@"theEmail"]];
    
    recipients = [recipients stringByAppendingString:@"?subject=re: SKiMP Item"];

    NSString *body = @"&body=Interested in: ";
    
    body = [body stringByAppendingString:theMessage];
    
    NSString *email = [NSString stringWithFormat:@"%@%@", recipients, body];
    email = [email stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:email]];
}


-(void)setPicture
{
    ImageURL = [[NSUserDefaults standardUserDefaults]
                stringForKey:@"imageURL"];
    
    NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:ImageURL]];
    
    self.pictureBox.image = [UIImage imageWithData:imageData];
    
    self.textLabel.text = theMessage;
    
    self.priceLabel.text = thePrice;
    
    self.titleLabel.text = theTitle;
    
    theEmail = [@"contact: " stringByAppendingString:theEmail];
    
    self.emailLabel.text = theEmail;
}

- (IBAction)sendEmail:(id)sender
{
    //[self launchMailAppOnDevice];
}
@end
