//
//  conversationsViewController.h
//  SKIMP
//
//  Created by Anan Mallik on 6/26/15.
//  Copyright (c) 2015 BookBoi Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface conversationsViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>
@end
