
//  EditSkimpViewController.m
//  SKIMP
//
//  Created by Levi mallik on 30/05/2015.
//  Copyright (c) 2015 BookBoi Inc. All rights reserved.

#import "EditSkimpViewController.h"

@interface EditSkimpViewController ()


@property (weak, nonatomic) IBOutlet UITextView *titleField;
@property (weak, nonatomic) IBOutlet UITextView *priceField;
@property (weak, nonatomic) IBOutlet UITextView *messageField;
@property (weak, nonatomic) IBOutlet UIImageView *pictureBox;
@property (weak, nonatomic) IBOutlet UIView *photoChoiceView;

- (IBAction)saveButton:(id)sender;
- (IBAction)selectImage:(id)sender;
- (IBAction)libraryButton:(id)sender;
- (IBAction)cameraButton:(id)sender;

@end

@implementation EditSkimpViewController

NSString *username;
NSString *ImageURL;
NSString *theMessage;
NSString *thePrice;
NSString *theUsername;
NSString *theSkimpID;
NSString *theTitle;
NSInteger randomNumber;

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    
    [self setPicture];
    
    self.pictureBox.layer.cornerRadius = 7.0;
    self.pictureBox.layer.shadowColor = [UIColor blackColor].CGColor;
    self.pictureBox.layer.shadowOffset = CGSizeMake(0, 2);
    self.pictureBox.layer.shadowOpacity = 0.5;
    self.pictureBox.layer.shadowRadius = 2.0;
    self.pictureBox.layer.masksToBounds = NO;
    
    [self setNeedsStatusBarAppearanceUpdate];

}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void)setPicture
{
    ImageURL = [[NSUserDefaults standardUserDefaults]
                stringForKey:@"imageURL"];
    
    NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:ImageURL]];
    
    self.pictureBox.image = [UIImage imageWithData:imageData];
    
    self.messageField.text = theMessage;
    
    self.priceField.text = thePrice;
    
    self.titleField.text = theTitle;
    
    self.pictureBox.layer.cornerRadius = 7.0;
    self.pictureBox.layer.shadowColor = [UIColor blackColor].CGColor;
    self.pictureBox.layer.shadowOffset = CGSizeMake(0, 2);
    self.pictureBox.layer.shadowOpacity = 0.5;
    self.pictureBox.layer.shadowRadius = 2.0;
    self.pictureBox.layer.masksToBounds = YES;

}


-(void)viewWillAppear:(BOOL)animated
{
    
    ImageURL = [[NSUserDefaults standardUserDefaults]
                stringForKey:@"imageURL"];
    
    theMessage = [[NSUserDefaults standardUserDefaults]
                  stringForKey:@"theMessage"];
    
    thePrice = [[NSUserDefaults standardUserDefaults]
                stringForKey:@"thePrice"];
    
    theUsername = [[NSUserDefaults standardUserDefaults]
                   stringForKey:@"theUsername"];
    
    theSkimpID = [[NSUserDefaults standardUserDefaults]
                  stringForKey:@"theSkimpID"];
    
    username = [[NSUserDefaults standardUserDefaults]
                stringForKey:@"myUsername"];
    
    theTitle = [[NSUserDefaults standardUserDefaults]
                stringForKey:@"theTitle"];
    
}

-(void)takePicture
{
    // Create image picker controller
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    
    // Set source to the camera
    imagePicker.sourceType =  UIImagePickerControllerSourceTypeCamera;
    
    // Delegate is self
    imagePicker.delegate = self;
    
    // Allow editing of image ?
    imagePicker.allowsImageEditing = YES;
    
    // Show image picker
    [self presentModalViewController:imagePicker animated:YES];
    
}


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    UIImage *image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
    
    // Save image
    UIImageWriteToSavedPhotosAlbum(image, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
    
    UIImage *selectedImage = info[UIImagePickerControllerEditedImage];
    self.pictureBox.image = selectedImage;
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}


- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    UIAlertView *alert;
    
    
    // Unable to save the image
    if (error)
        alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                           message:@"Unable to save image to Photo Album."
                                          delegate:self cancelButtonTitle:@"Ok"
                                 otherButtonTitles:nil];
    else // All is well
        alert = [[UIAlertView alloc] initWithTitle:@"Success"
                                           message:@"Image saved to Photo Album."
                                          delegate:self cancelButtonTitle:@"Ok"
                                 otherButtonTitles:nil];
}

-(void)upLoad
{
    
    randomNumber = arc4random() % 9999999999;
    
    NSLog(@"%ld", (long)randomNumber);
    
    NSString *random = [NSString stringWithFormat:@"%ld", (long)randomNumber];
    
    NSData *imageData = UIImageJPEGRepresentation(self.pictureBox.image, 0.2);
    NSString *urlString = @"http://52.24.55.43/b9ef165b255673dde47bff07f4390fb1/upload.php";
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:urlString]];
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = @"---------------------------14737809831466499882746641449"
    ;
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    NSMutableData *body = [NSMutableData data];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *filename = @"Content-Disposition: form-data; name=\"userfile\"; filename=\"";
    
    filename  = [filename stringByAppendingString:random];
    
    filename  = [filename stringByAppendingString:@".jpg\"\r\n"];
    
    [body appendData:[filename dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[NSData dataWithData:imageData]];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPBody:body];
    
    NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
    NSLog(@"heyo");
    
}


-(void)attachFile
{
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:picker animated:YES completion:NULL];
    
    
}

- (IBAction)saveButton:(id)sender
{
    
    [self upLoad];
    
    [self upDate];
    
}


-(void)upDate
{
        
        NSString *email; NSString *phone; NSString *username;
        
        email = [[NSUserDefaults standardUserDefaults]
                 stringForKey:@"myEmail"];
        
        phone = [[NSUserDefaults standardUserDefaults]
                 stringForKey:@"myPhone"];
        
        username = [[NSUserDefaults standardUserDefaults]
                    stringForKey:@"myUsername"];
        
        NSString *url = @"http://52.24.55.43/b9ef165b255673dde47bff07f4390fb1/editskimp.php?skimpid=";
        
        url = [url stringByAppendingString:theSkimpID];
        url = [url stringByAppendingString:@"&message="];
        url = [url stringByAppendingString:self.messageField.text];
        url = [url stringByAppendingString:@"&price="];
        url = [url stringByAppendingString:self.priceField.text];
        url = [url stringByAppendingString:@"&title="];
        url = [url stringByAppendingString:self.titleField.text];
        
        NSString *random = [NSString stringWithFormat:@"%ld", (long)randomNumber];
        
        url = [url stringByAppendingString:@"&imageid="];
        url = [url stringByAppendingString:random];
        
        url = [url stringByAppendingString:@".jpg"];
        
        url = [url stringByReplacingOccurrencesOfString:@" "
                                             withString:@"%20"];
        url = [url stringByReplacingOccurrencesOfString:@" "
                                             withString:@"%20"];
        url = [url stringByReplacingOccurrencesOfString:@" "
                                             withString:@"%20"];
        url = [url stringByReplacingOccurrencesOfString:@" "
                                             withString:@"%20"];
        
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
                                                               cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                                           timeoutInterval:100];
        
        NSError *requestError;
        NSURLResponse *urlResponse = nil;
        
        NSData *response = [NSURLConnection sendSynchronousRequest:request
                                                 returningResponse:&urlResponse error:&requestError];
        
        NSLog(@"%@",response);
        
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Success!" message:@"Your Skimp has be Updated!." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        
        
        [self performSegueWithIdentifier:@"returndetail" sender:self];
    
}



- (IBAction)selectImage:(id)sender
{
    
    [self.messageField resignFirstResponder];
    
}

- (IBAction)libraryButton:(id)sender
{
    
    [self attachFile];
    
}

- (IBAction)cameraButton:(id)sender
{
    
    [self takePicture];
    
}
@end
