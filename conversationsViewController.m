//
//  conversationsViewController.m
//  SKIMP
//
//  Created by Anan Mallik on 6/26/15.
//  Copyright (c) 2015 BookBoi Inc. All rights reserved.
//

#import "conversationsViewController.h"


@interface conversationsViewController()


@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) NSMutableArray *tableData1;
@property (strong, nonatomic) NSMutableArray *tableData2;
@property (strong, nonatomic) NSMutableArray *tableData3;
@property (strong, nonatomic) NSMutableArray *tableData4;

@property (weak, nonatomic) IBOutlet UILabel *noconv;

@end



@implementation conversationsViewController

NSInteger rowNum;
NSInteger tableIndex;

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    
    [self getConversations];

    if(self.tableData1.count == 0)
    {
        [self.noconv setHidden:FALSE];
        [self.tableView setHidden:TRUE];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    tableIndex = indexPath.row;
    
    NSLog(@"%@",self.tableData4[tableIndex]);
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    [defaults setObject:self.tableData4[tableIndex] forKey:@"convID"];
    
    [self performSegueWithIdentifier:@"pickConv" sender:self];
}



-(void)getConversations
{
    
    self.tableData1 = [[NSMutableArray alloc] init];
    self.tableData2 = [[NSMutableArray alloc] init];
    self.tableData3 = [[NSMutableArray alloc] init];
    self.tableData4 = [[NSMutableArray alloc] init];
    
    NSString *username = [[NSUserDefaults standardUserDefaults]
                stringForKey:@"myUsername"];
    
    
    NSString *url = @"http://52.24.55.43/myconversations.php?username=";
    

    url = [url stringByAppendingString:username];

    
    NSURL *theurl = [NSURL URLWithString:url];
    NSURLRequest *request = [NSURLRequest requestWithURL:theurl];
    NSURLResponse *response;
    NSError *error;
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    NSArray *skimps = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableContainers error:nil];
    
    
    for (int i=0; i<skimps.count; i++)
    {
        [self.tableData1 addObject:skimps[i][@"username"]];
    }
    
    for (int i=0; i<skimps.count; i++)
    {
        [self.tableData2 addObject:skimps[i][@"title"]];
    }
    
    for (int i=0; i<skimps.count; i++)
    {
        [self.tableData3 addObject:skimps[i][@"imageid"]];
    }
    
    for (int i=0; i<skimps.count; i++)
    {
        [self.tableData4 addObject:skimps[i][@"convid"]];
    }
    
    rowNum = [self.tableData1 count];
    
    [self.tableView reloadData];
    
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 95;
    
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return rowNum;
    
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellIdentifier = @"skimpCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    UIImageView *image = (UIImageView *)[cell.contentView viewWithTag:7];
    
    UILabel *titleLabel = (UILabel *)[cell.contentView viewWithTag:3];
    UILabel *subtitleLabel = (UILabel *)[cell.contentView viewWithTag:1];
    
    NSString *title = [self.tableData1 objectAtIndex:indexPath.row];
    
    [subtitleLabel setText:[self.tableData2 objectAtIndex:indexPath.row]];
    
    [titleLabel setText: title];
    
    image.layer.cornerRadius = 7.0;
    image.layer.masksToBounds = YES;
    
    //Cool Image Setter Code---->>>>>>>>>>>>>>
    NSString *ImageURL = @"http://52.24.55.43/b9ef165b255673dde47bff07f4390fb1/pictures/";
    
    ImageURL = [ImageURL stringByAppendingString:[self.tableData3 objectAtIndex:indexPath.row]];
    
    NSURL *imageURL = [NSURL URLWithString:ImageURL];
    
    image.image = [UIImage imageNamed:@"logo.png"];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        
        sleep(0.2);
        
        NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            UIImage *setImage = [UIImage imageWithData:imageData];
            [image setImage:setImage];
        });
    });

    
    return cell;
    
}


@end
