//
//  ViewController.m
//  BookBoi_iPhone
//
//  Created by Anan Mallik on 7/4/14.
//  Copyright (c) 2014 BookBoi Inc. All rights reserved.
//

#import "ViewController.h"
#import "Reachability.h"
#import <CoreLocation/CoreLocation.h>


@interface ViewController ()


@property (weak, nonatomic) IBOutlet UITextView *terms;
@property (weak, nonatomic) IBOutlet UIView *termsView;

@property (weak, nonatomic) IBOutlet UIButton *button1;
@property (weak, nonatomic) IBOutlet UIButton *button2;
@property (weak, nonatomic) IBOutlet UILabel *label1;
@property (weak, nonatomic) IBOutlet UILabel *label2;

- (IBAction)closeTerms:(id)sender;
@property (retain, nonatomic) UIAlertView* alertView;
@end

@implementation ViewController

NSString *latitude;
NSString *longitude;
CLLocationManager *locationManager;
bool flag;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self getusername];
    
    [self dropShadow];
	// Do any additional setup after loading the view, typically from a nib.
    
    self.terms.text = @"User Guidelines:\n\n1. Do not post your or anyones personal 'address' inside a SKiMP. A business address is fine.\n\n2. Try to conduct SKiMP transactions in public or with other people.\n\n3. Do not  'harass' people you meet using SKiMP.\n\n4. Use Cash/PayPal/Venmo/SnapCash to complete SKiMP transactions. Do not use or accept personal checks or Western Union.\n\n5. SKiMP is NOT a dating app.\n\n6. Do not use this app to create spam SKiMPs or unsolicitated ads.\n\n7. All transactions should be considered final. No returns or exchanges.\n\n8. Inform abuse of these guidelines to skimpapp@gmail.com.\n\n";
    
    self.terms.textColor = [UIColor whiteColor];
    self.terms.textAlignment = NSTextAlignmentJustified;

    [self setNeedsStatusBarAppearanceUpdate];
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate=self;
    locationManager.desiredAccuracy=kCLLocationAccuracyBest;
    locationManager.distanceFilter=kCLDistanceFilterNone;
    [locationManager requestWhenInUseAuthorization];
    [locationManager startMonitoringSignificantLocationChanges];
    [locationManager startUpdatingLocation];

}

-(void)dropShadow
{
    self.button1.layer.cornerRadius = 7.0;
    self.button1.layer.shadowColor = [UIColor blackColor].CGColor;
    self.button1.layer.shadowOffset = CGSizeMake(0, 2);
    self.button1.layer.shadowOpacity = 0.5;
    self.button1.layer.shadowRadius = 2.0;
    self.button1.layer.masksToBounds = NO;
    
    self.button2.layer.cornerRadius = 7.0;
    self.button2.layer.shadowColor = [UIColor blackColor].CGColor;
    self.button2.layer.shadowOffset = CGSizeMake(0, 2);
    self.button2.layer.shadowOpacity = 0.5;
    self.button2.layer.shadowRadius = 2.0;
    self.button2.layer.masksToBounds = NO;
}

-(void)getusername
{
    NSString *email = [[NSUserDefaults standardUserDefaults]
                       stringForKey:@"myEmail"];
    
    NSString *baseURL = @"http://52.24.55.43/b9ef165b255673dde47bff07f4390fb1/myprofile.php?email=";
    
    baseURL = [baseURL stringByAppendingString:email];
    
    NSURL *url = [NSURL URLWithString:baseURL];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLResponse *response;
    NSError *error;
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    NSArray *skimps = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableContainers error:nil];
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    [defaults setObject:skimps[0][@"username"] forKey:@"myUsername"];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:TRUE];
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate=self;
    locationManager.desiredAccuracy=kCLLocationAccuracyBest;
    locationManager.distanceFilter=kCLDistanceFilterNone;
    [locationManager requestWhenInUseAuthorization];
    [locationManager startMonitoringSignificantLocationChanges];
    [locationManager startUpdatingLocation];
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorAlert show];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    locationManager = [[CLLocationManager alloc] init];
    [locationManager requestWhenInUseAuthorization];
    
    //NSLog(@"didUpdateToLocation: %@", newLocation);
    CLLocation *currentLocation = newLocation;
    
    
    if (currentLocation != nil)
    {
        longitude = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude];
        latitude = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude];
    }
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)closeTerms:(id)sender
{
    [UIView animateWithDuration:0.5
                     animations:^{
                         [self.termsView setFrame:CGRectMake(40,-810,self.termsView.frame.size.width, self.termsView.frame.size.height)];
                     }];
    
    flag = true;
}
@end
