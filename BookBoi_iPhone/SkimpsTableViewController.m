//
//  SkimpsTableViewController.m
//  SKIMP
//
//  Created by Gerald Fairclough on 3/21/15.
//  Copyright (c) 2015 BookBoi Inc. All rights reserved.

#import "SkimpsTableViewController.h"
#import <MessageUI/MessageUI.h>
#import <CoreLocation/CoreLocation.h>
#import "DropDownListView.h"

@interface SkimpsTableViewController()

//implement any properties here
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIPickerView *pickerView;
@property (weak, nonatomic) IBOutlet UILabel *myMiles;
@property (weak, nonatomic) IBOutlet UIView *distanceView;
@property (weak, nonatomic) IBOutlet UIImageView *darkLayer;
@property (weak, nonatomic) IBOutlet UILabel *lblSelectedCategoryNames;
@property (strong, nonatomic) NSMutableArray *tableData1;
@property (strong, nonatomic) NSMutableArray *tableData2;
@property (strong, nonatomic) NSMutableArray *tableData3;
@property (strong, nonatomic) NSMutableArray *tableData4;
@property (strong, nonatomic) NSMutableArray *tableData5;
@property (strong, nonatomic) NSMutableArray *tableData7;
@property (strong, nonatomic) NSMutableArray *tableData8;
@property (strong, nonatomic) NSMutableArray *tableData9;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (nonatomic, strong) NSArray *arryList;
@property (strong, nonatomic) NSMutableArray *tableID;
@property (weak, nonatomic) IBOutlet UIButton *skimp;

- (IBAction)dropDownPress:(id)sender;
- (IBAction)setMiles:(id)sender;
- (IBAction)openPicker:(id)sender;

@end

@implementation SkimpsTableViewController

NSString *detail;
NSString *distance = @"50";
NSInteger rowNum;
NSInteger tableIndex;
NSArray *skimps;
NSString *myLat;
NSString *myLong;
MFMessageComposeViewController *messageController;
NSString *ImageURL;
NSString *cat = @"all";
NSArray *mySortedArray;
bool didWrite = FALSE;
NSString *theMessage;
NSString *thePrice;
NSString *theUsername;
NSString *theEmail;
NSString *theSkimpID;
NSString *theTitle;
UIActivityIndicatorView *spinner;
NSMutableData* responseData;
NSArray *_pickerData;

- (void)viewDidLoad
{
    distance = @"50";
    
    cat = @"all";
    
    [self populateData:distance];

    self.pickerView.layer.cornerRadius = 7.0;
    self.pickerView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.pickerView.layer.shadowOffset = CGSizeMake(0, 2);
    self.pickerView.layer.shadowOpacity = 0.5;
    self.pickerView.layer.shadowRadius = 2.0;
    self.pickerView.layer.masksToBounds = NO;
    
    [super viewDidLoad];
    
    _pickerData = @[@"1 mile", @"5 miles", @"10 miles", @"25 miles", @"50 miles", @"max"];
    
    [self spinme];
    
    [self getusername];
    
    [self setNeedsStatusBarAppearanceUpdate];
    
    self.arryList = @[@"Movies, Books & Games", @"Technology",@"Cars & Autos",@"Housing",@"Sports & Leisure Eqp.", @"Fashion & Accessories",@"Events",@"Misc.", @"Tutoring"];
    
    self.searchBar.delegate = self;
    
    self.tableData5 = [[NSMutableArray alloc] init];
    self.tableData7 = [[NSMutableArray alloc] init];
}


// The number of columns of data
- (int)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// The number of rows of data
- (int)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return _pickerData.count;
}

// The data to return for the row and component (column) that's being passed in
- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    
    return _pickerData[row];

}


// Catpure the picker view selection
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
       NSLog(@"%@", _pickerData[row]);
    
       if([_pickerData[row] isEqualToString:@"0.5 miles"])
        {
            distance = @"0.5";
            self.myMiles.text = @"0.5 mi";
        }
        if([_pickerData[row] isEqualToString:@"1 mile"])
        {
            distance = @"1";
            self.myMiles.text = @"1 mi";
        }
        if([_pickerData[row] isEqualToString:@"5 miles"])
        {
            distance = @"5";
            self.myMiles.text = @"5 mi";
        }
        if([_pickerData[row] isEqualToString:@"10 miles"])
        {
            distance = @"10";
            self.myMiles.text = @"10 mi";
        }
        if([_pickerData[row] isEqualToString:@"25 miles"])
        {
            distance = @"25";
            self.myMiles.text = @"25 mi";
        }
        if([_pickerData[row] isEqualToString:@"50 miles"])
        {
            distance = @"50";
            self.myMiles.text = @"50 mi";
        }
        if([_pickerData[row] isEqualToString:@"max"])
        {
            distance = @"25000";
            self.myMiles.text = @"max";
        }
}


-(void)spinme
{
    spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    spinner.frame = CGRectMake(100.00, 250.00, 50.0, 50.0);
    
    spinner.center = self.view.center;
    
    [self.view addSubview:spinner];
    
    [spinner startAnimating];
    
    //Grand Dispatch is Awesome!
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{

        sleep(1.5);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [spinner stopAnimating];
            
        });
    });
}



-(void)setPicture
{
    ImageURL = @"http://52.24.55.43/b9ef165b255673dde47bff07f4390fb1/pictures/";
    
    ImageURL = [ImageURL stringByAppendingString:[self.tableData4 objectAtIndex:tableIndex]];
    
    theMessage = skimps[tableIndex][@"message"];
    
    thePrice = skimps[tableIndex][@"price"];
    
    theUsername = skimps[tableIndex][@"username"];
    
    theSkimpID = skimps[tableIndex][@"id"];
    
    theTitle = skimps[tableIndex][@"title"];
    
    theEmail = skimps[tableIndex][@"email"];
}

-(void)viewWillDisappear:(BOOL)animated
{
    if(self.tableData1.count != 0)
    {
        [self setPicture];
    
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
        [defaults setObject:ImageURL forKey:@"imageURL"];
        [defaults setObject:theMessage forKey:@"theMessage"];
        [defaults setObject:thePrice forKey:@"thePrice"];
        [defaults setObject:theUsername forKey:@"theUsername"];
        [defaults setObject:theSkimpID forKey:@"theSkimpID"];
        [defaults setObject:theTitle forKey:@"theTitle"];
        [defaults setObject:theEmail forKey:@"theEmail"];
    }
}

-(void)closePicker
{
    
    [UIView transitionWithView:self.distanceView
                      duration:0.4
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:NULL
                    completion:NULL];
    
    self.distanceView.hidden = YES;
    
    [UIView transitionWithView:self.darkLayer
                      duration:0.4
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:NULL
                    completion:NULL];
    
    
    [self.darkLayer setHidden: YES];
}


-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void)viewWillAppear:(BOOL)animated
{
    
    myLat = [[NSUserDefaults standardUserDefaults]
             stringForKey:@"myLat"];
    myLong = [[NSUserDefaults standardUserDefaults]
              stringForKey:@"myLong"];
    
    [self.pickerView selectRow:4 inComponent:0 animated:YES];
    
}

-(void)populateData: (NSString *) dist
{
    
    myLat = [[NSUserDefaults standardUserDefaults]
             stringForKey:@"myLat"];
    myLong = [[NSUserDefaults standardUserDefaults]
              stringForKey:@"myLong"];
    
    self.tableData1 = [[NSMutableArray alloc] init];
    self.tableData2 = [[NSMutableArray alloc] init];
    self.tableData3 = [[NSMutableArray alloc] init];
    self.tableData4 = [[NSMutableArray alloc] init];
    self.tableData8 = [[NSMutableArray alloc] init];
    self.tableID = [[NSMutableArray alloc] init];
    
    NSString *baseURL = @"http://52.24.55.43/b9ef165b255673dde47bff07f4390fb1/distance.php?lat=";
    
    baseURL = [baseURL stringByAppendingString:myLat];
    baseURL = [baseURL stringByAppendingString:@"&long="];
    baseURL = [baseURL stringByAppendingString:myLong];
    baseURL = [baseURL stringByAppendingString:@"&miles="];
    baseURL = [baseURL stringByAppendingString:dist];
    baseURL = [baseURL stringByAppendingString:@"&data="];
    baseURL = [baseURL stringByAppendingString:cat];
    
    baseURL = [baseURL stringByReplacingOccurrencesOfString:@" "
                                                 withString:@"%20"];
    baseURL = [baseURL stringByReplacingOccurrencesOfString:@" "
                                                 withString:@"%20"];
    baseURL = [baseURL stringByReplacingOccurrencesOfString:@" "
                                                 withString:@"%20"];
    baseURL = [baseURL stringByReplacingOccurrencesOfString:@" "
                                                 withString:@"%20"];
    
    NSURL *url = [NSURL URLWithString:baseURL];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLResponse *response;
    NSError *error;
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    skimps = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableContainers error:nil];
    
    
    for (int i=0; i<skimps.count; i++)
    {
        [self.tableData1 addObject:skimps[i][@"title"]];
    }
    
    for (int i=0; i<skimps.count; i++)
    {
        [self.tableData4 addObject:skimps[i][@"imageid"]];
    }
    
    for (int i=0; i<skimps.count; i++)
    {
        [self.tableID addObject:skimps[i][@"id"]];
    }
    
    for (int i=0; i<skimps.count; i++)
    {
        [self.tableData8 addObject:skimps[i][@"price"]];
    }
    
    for (int i=0; i<skimps.count; i++)
    {
        NSString *gps = skimps[i][@"gps"];
        
        NSArray *split = [gps componentsSeparatedByString: @","];
        
        CLLocation *locA = [[CLLocation alloc] initWithLatitude:(CGFloat)[split[0] floatValue] longitude:(CGFloat)[split[1] floatValue]];
        
        CLLocation *locB = [[CLLocation alloc] initWithLatitude:(CGFloat)[myLat floatValue] longitude:(CGFloat)[myLong floatValue]];
        
        CLLocationDistance distance = [locA distanceFromLocation:locB];
        
        distance = distance/1609.34;
        
        [self.tableData3 addObject:[NSString stringWithFormat:@"%0.1f mile", distance]];
        [self.tableData5 addObject:[NSString stringWithFormat:@"%0.1f mile", distance]];
    }
    
    for (int i=0; i<skimps.count; i++)
    {
        [self.tableData2 addObject:skimps[i][@"message"]];
    }
    
    [self.tableView reloadData];
    
    rowNum = [self.tableData1 count];
    
    [self.tableView reloadData];
    
    rowNum = [self.tableData1 count];
}


-(void)populateSearchData
{
    
    myLat = [[NSUserDefaults standardUserDefaults]
             stringForKey:@"myLat"];
    myLong = [[NSUserDefaults standardUserDefaults]
              stringForKey:@"myLong"];
    
    [self.tableData1 removeAllObjects];
    [self.tableData2 removeAllObjects];
    [self.tableData3 removeAllObjects];
    [self.tableData4 removeAllObjects];
    [self.tableData8 removeAllObjects];
    [self.tableID removeAllObjects];
    
    NSString *baseURL = @"http://52.24.55.43/b9ef165b255673dde47bff07f4390fb1/searchskimps.php?data=";
    
    baseURL = [baseURL stringByAppendingString:self.searchBar.text];
    
    baseURL = [baseURL stringByAppendingString:@"&lat="];
    baseURL = [baseURL stringByAppendingString:myLat];
    baseURL = [baseURL stringByAppendingString:@"&long="];
    baseURL = [baseURL stringByAppendingString:myLong];
    baseURL = [baseURL stringByAppendingString:@"&miles="];
    baseURL = [baseURL stringByAppendingString:distance];
    
    baseURL = [baseURL stringByReplacingOccurrencesOfString:@" "
                                                   withString:@"%20"];
    baseURL = [baseURL stringByReplacingOccurrencesOfString:@" "
                                                   withString:@"%20"];
    baseURL = [baseURL stringByReplacingOccurrencesOfString:@" "
                                                 withString:@"%20"];
    baseURL = [baseURL stringByReplacingOccurrencesOfString:@" "
                                                 withString:@"%20"];
    baseURL = [baseURL stringByReplacingOccurrencesOfString:@" "
                                                 withString:@"%20"];
    baseURL = [baseURL stringByReplacingOccurrencesOfString:@" "
                                                 withString:@"%20"];

    
    NSURL *url = [NSURL URLWithString:baseURL];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLResponse *response;
    NSError *error;
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    skimps = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableContainers error:nil];
    
    if(skimps.count == 0)
    {
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Alert" message:@"Your search had no matches." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    
    for (int i=0; i<skimps.count; i++)
    {
        [self.tableData1 addObject:skimps[i][@"title"]];
    }
    
    for (int i=0; i<skimps.count; i++)
    {
        [self.tableData4 addObject:skimps[i][@"imageid"]];
    }
    
    for (int i=0; i<skimps.count; i++)
    {
        [self.tableID addObject:skimps[i][@"id"]];
    }
    
    for (int i=0; i<skimps.count; i++)
    {
        [self.tableData8 addObject:skimps[i][@"price"]];
    }
    
    for (int i=0; i<skimps.count; i++)
    {
        NSString *gps = skimps[i][@"gps"];
        
        NSArray *split = [gps componentsSeparatedByString: @","];
        
        CLLocation *locA = [[CLLocation alloc] initWithLatitude:(CGFloat)[split[0] floatValue] longitude:(CGFloat)[split[1] floatValue]];
        
        CLLocation *locB = [[CLLocation alloc] initWithLatitude:(CGFloat)[myLat floatValue] longitude:(CGFloat)[myLong floatValue]];
        
        CLLocationDistance distance = [locA distanceFromLocation:locB];
        
        distance = distance/1609.34;
        
        [self.tableData3 addObject:[NSString stringWithFormat:@"%0.1f mile", distance]];
    }
    
    for (int i=0; i<skimps.count; i++)
    {
        [self.tableData2 addObject:skimps[i][@"message"]];
    }
    
    [self.tableView reloadData];

    rowNum = [self.tableData1 count];
    
    [self.tableView reloadData];
    
    rowNum = [self.tableData1 count];
}

-(void)getusername
{
    NSString *email = [[NSUserDefaults standardUserDefaults]
                       stringForKey:@"myEmail"];
    
    NSString *baseURL = @"http://52.24.55.43/b9ef165b255673dde47bff07f4390fb1/myprofile.php?email=";
    
    baseURL = [baseURL stringByAppendingString:email];
    
    NSURL *url = [NSURL URLWithString:baseURL];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLResponse *response;
    NSError *error;
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    NSArray *skimps = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableContainers error:nil];
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    [defaults setObject:skimps[0][@"username"] forKey:@"myUsername"];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self spinme];
    
    tableIndex = indexPath.row;
    
    [self setPicture];
    
    [self performSegueWithIdentifier:@"godetail" sender:self];
    
}



- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [self.searchBar resignFirstResponder];
    
    [self populateSearchData];
}


-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 400;
    
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return rowNum;
    
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [self.searchBar resignFirstResponder];
    
    [UIView animateWithDuration:0.5
                     animations:^{
                         [self.skimp setFrame:CGRectMake(0,600,self.skimp.frame.size.width, self.skimp.frame.size.height)];
                     }];
    
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{

    
    
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellIdentifier = @"skimpCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    UILabel *titleLabel = (UILabel *)[cell.contentView viewWithTag:3];
    UILabel *subtitleLabel = (UILabel *)[cell.contentView viewWithTag:1];
    UILabel *ageLabel = (UILabel *)[cell.contentView viewWithTag:2];
    
    UILabel *priceLabel = (UILabel *)[cell.contentView viewWithTag:9];
    
    UIImageView *image = (UIImageView *)[cell.contentView viewWithTag:7];
    
    UIImageView *curve = (UIImageView *)[cell.contentView viewWithTag:8];
    
    NSString *title = [self.tableData1 objectAtIndex:indexPath.row];
    
    [subtitleLabel setText:[self.tableData2 objectAtIndex:indexPath.row]];
    
    [ageLabel setText:[self.tableData3 objectAtIndex:indexPath.row]];
    
    [priceLabel setText:[self.tableData8 objectAtIndex:indexPath.row]];
    
    [titleLabel setText: title];
    
    image.layer.cornerRadius = 7.0;
    image.layer.shadowColor = [UIColor blackColor].CGColor;
    image.layer.shadowOffset = CGSizeMake(0, 2);
    image.layer.shadowOpacity = 0.5;
    image.layer.shadowRadius = 2.0;
    image.layer.masksToBounds = NO;
    
    curve.layer.cornerRadius = 7.0;
    curve.layer.shadowColor = [UIColor blackColor].CGColor;
    curve.layer.shadowOffset = CGSizeMake(0, 2);
    curve.layer.shadowOpacity = 0.5;
    curve.layer.shadowRadius = 2.0;
    curve.layer.masksToBounds = YES;
    
    image.layer.cornerRadius = 7.0;
    curve.layer.cornerRadius = 9.0;

    //Cool Image Setter Code---->>>>>>>>>>>>>>
    ImageURL = @"http://52.24.55.43/b9ef165b255673dde47bff07f4390fb1/pictures/";
    ImageURL = [ImageURL stringByAppendingString:[self.tableData4 objectAtIndex:indexPath.row]];
    NSURL *imageURL = [NSURL URLWithString:ImageURL];
    
    image.image = [UIImage imageNamed:@"logo.png"];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        
            NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
             UIImage *setImage = [UIImage imageWithData:imageData];
             [image setImage:setImage];
             [image setNeedsLayout];
            
        });
    });
 
    return cell;
}


- (IBAction)dropDownPress:(id)sender
{
    
    [Dropobj fadeOut];
    [self showPopUpWithTitle:@"Sort by Category" withOption:self.arryList xy:CGPointMake(16, 80) size:CGSizeMake(287, 445) isMultiple:NO];
    
}

- (IBAction)setMiles:(id)sender
{
    [self populateData: distance];
    
    [self closePicker];
}

- (IBAction)openPicker:(id)sender
{
    [UIView transitionWithView:self.distanceView
                      duration:0.3
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:NULL
                    completion:NULL];
    
    self.distanceView.hidden = NO;
    
    [UIView transitionWithView:self.darkLayer
                      duration:0.3
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:NULL
                    completion:NULL];
    
    
    [self.darkLayer setHidden: NO];
    
    if([distance isEqualToString:@"25000"])
        self.myMiles.text = @"max";
    else
        self.myMiles.text = [distance stringByAppendingString:@" mi"];
}

-(void)showPopUpWithTitle:(NSString*)popupTitle withOption:(NSArray*)arrOptions xy:(CGPoint)point size:(CGSize)size isMultiple:(BOOL)isMultiple
{
    
    Dropobj = [[DropDownListView alloc] initWithTitle:popupTitle options:arrOptions xy:point size:size isMultiple:isMultiple];
    Dropobj.delegate = self;
    [Dropobj showInView:self.view animated:YES];
    
    /*----------------Set DropDown backGroundColor-----------------*/
    [Dropobj SetBackGroundDropDwon_R:0.0 G:108.0 B:194.0 alpha:0.70];
    
}

- (void)DropDownListView:(DropDownListView *)dropdownListView didSelectedIndex:(NSInteger)anIndex
{
    
    if([[self.arryList objectAtIndex:anIndex] isEqualToString:@"Movies, Books & Games"])
    {
        cat = @"Movies, Books & Games";
    }
    if([[self.arryList objectAtIndex:anIndex] isEqualToString:@"Cars & Autos"])
    {
        cat = @"Cars & Autos";
    }
    if([[self.arryList objectAtIndex:anIndex] isEqualToString:@"Sports & Leisure Eqp."])
    {
        cat = @"Sports & Leisure Eqp.";
    }
    if([[self.arryList objectAtIndex:anIndex] isEqualToString:@"Technology"])
    {
        cat = @"Technology";
    }
    if([[self.arryList objectAtIndex:anIndex] isEqualToString:@"Misc."])
    {
        cat = @"Misc.";
    }
    if([[self.arryList objectAtIndex:anIndex] isEqualToString:@"Events"])
    {
        cat = @"Events";
    }
    if([[self.arryList objectAtIndex:anIndex] isEqualToString:@"Housing"])
    {
        cat = @"Housing";
    }
    if([[self.arryList objectAtIndex:anIndex] isEqualToString:@"Fashion & Accessories"])
    {
        cat = @"Fashion & Accessories";
    }
    if([[self.arryList objectAtIndex:anIndex] isEqualToString:@"Tutoring"])
    {
        cat = @"Tutoring";
    }
    
    
    myLat = [[NSUserDefaults standardUserDefaults]
             stringForKey:@"myLat"];
    myLong = [[NSUserDefaults standardUserDefaults]
              stringForKey:@"myLong"];
    
    
    self.tableData1 = [[NSMutableArray alloc] init];
    self.tableData2 = [[NSMutableArray alloc] init];
    self.tableData3 = [[NSMutableArray alloc] init];
    self.tableData4 = [[NSMutableArray alloc] init];
    self.tableData8 = [[NSMutableArray alloc] init];
    self.tableID = [[NSMutableArray alloc] init];
    
    NSString *baseURL;
    
    
    baseURL = @"http://52.24.55.43/b9ef165b255673dde47bff07f4390fb1/distance.php?data=";
       
    
    baseURL = [baseURL stringByAppendingString:cat];
    baseURL = [baseURL stringByAppendingString:@"&lat="];
    baseURL = [baseURL stringByAppendingString:myLat];
    baseURL = [baseURL stringByAppendingString:@"&long="];
    baseURL = [baseURL stringByAppendingString:myLong];
    baseURL = [baseURL stringByAppendingString:@"&miles="];
    baseURL = [baseURL stringByAppendingString:distance];
    
    baseURL = [baseURL stringByReplacingOccurrencesOfString:@" "
                                                 withString:@"%20"];
    baseURL = [baseURL stringByReplacingOccurrencesOfString:@" "
                                                 withString:@"%20"];
    baseURL = [baseURL stringByReplacingOccurrencesOfString:@" "
                                                 withString:@"%20"];
    baseURL = [baseURL stringByReplacingOccurrencesOfString:@" "
                                                 withString:@"%20"];
    baseURL = [baseURL stringByReplacingOccurrencesOfString:@" "
                                                 withString:@"%20"];
    baseURL = [baseURL stringByReplacingOccurrencesOfString:@" "
                                                 withString:@"%20"];
    
    NSURL *url = [NSURL URLWithString:baseURL];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLResponse *response;
    NSError *error;
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    skimps = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableContainers error:nil];
    
    
    for (int i=0; i<skimps.count; i++)
    {
        [self.tableData1 addObject:skimps[i][@"title"]];
    }
    
    for (int i=0; i<skimps.count; i++)
    {
        NSString *gps = skimps[i][@"gps"];
        
        NSArray *split = [gps componentsSeparatedByString: @","];
        
        CLLocation *locA = [[CLLocation alloc] initWithLatitude:(CGFloat)[split[0] floatValue] longitude:(CGFloat)[split[1] floatValue]];
        
        CLLocation *locB = [[CLLocation alloc] initWithLatitude:(CGFloat)[myLat floatValue] longitude:(CGFloat)[myLong floatValue]];
        
        CLLocationDistance distance = [locA distanceFromLocation:locB];
        
        distance = distance/1609.34;
        
        [self.tableData3 addObject:[NSString stringWithFormat:@"%0.1f mile", distance]];
        
    }
    
    for (int i=0; i<skimps.count; i++)
    {
        [self.tableData2 addObject:skimps[i][@"message"]];
    }
    
    for (int i=0; i<skimps.count; i++)
    {
        [self.tableData4 addObject:skimps[i][@"imageid"]];
    }
    
    for (int i=0; i<skimps.count; i++)
    {
        [self.tableID addObject:skimps[i][@"id"]];
    }
    
    for (int i=0; i<skimps.count; i++)
    {
        [self.tableData8 addObject:skimps[i][@"price"]];
    }
    
    [self.tableView reloadData];
    
    rowNum = [self.tableData1 count];
    
    [self.tableView reloadData];
    
    rowNum = [self.tableData1 count];
}


- (void)DropDownListView:(DropDownListView *)dropdownListView Datalist:(NSMutableArray*)ArryData
{
    
    /*----------------Get Selected Value[Multiple selection]-----------------*/
    if (ArryData.count>0)
    {
        _lblSelectedCategoryNames.text=[ArryData componentsJoinedByString:@"\n"];
        CGSize size=[self GetHeightDyanamic:_lblSelectedCategoryNames];
        _lblSelectedCategoryNames.frame=CGRectMake(16, 240, 287, size.height);
    }
    else
    {
        _lblSelectedCategoryNames.text=@"";
    }
    
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    
    if ([touch.view isKindOfClass:[UIView class]])
    {
        [Dropobj fadeOut];
    }
}


-(CGSize)GetHeightDyanamic:(UILabel*)lbl
{
    NSRange range = NSMakeRange(0, [lbl.text length]);
    CGSize constraint;
    constraint= CGSizeMake(288 ,MAXFLOAT);
    CGSize size;
    
    if (([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0))
    {
        NSDictionary *attributes = [lbl.attributedText attributesAtIndex:0 effectiveRange:&range];
        CGSize boundingBox = [lbl.text boundingRectWithSize:constraint options: NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil].size;
        
        size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
    }
    else
    {
        size = [lbl.text sizeWithFont:[UIFont fontWithName:@"Helvetica" size:14] constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping];
    }
    return size;
}


@end