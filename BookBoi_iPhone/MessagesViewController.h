//
//  MessagesViewController.h
//  InfiniteMessage
//
//  Created by Levi mallik on 26/05/2015.
//  Copyright (c) 2015 BookBoi Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MessagesViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>
@end
