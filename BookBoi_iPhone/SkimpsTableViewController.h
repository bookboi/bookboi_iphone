//
//  SkimpsTableViewController.h
//  SKIMP
//
//  Created by Gerald Fairclough on 3/21/15.
//  Copyright (c) 2015 BookBoi Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DropDownListView.h"


@interface SkimpsTableViewController : UIViewController<kDropDownListViewDelegate,UITableViewDelegate, UITableViewDataSource, UIPickerViewDataSource, UIPickerViewDelegate>
{
    
    DropDownListView * Dropobj;
    
}

@end
