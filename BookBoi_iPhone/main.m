//  main.m
//  BookBoi_iPhone
//
//  Created by Anan Mallik on 7/4/14.
//  Copyright (c) 2014 BookBoi Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool
    {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
