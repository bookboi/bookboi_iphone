//
//  ProfileViewController.m
//  BookBoi_iPhone
//
//  Created by Anan Mallik on 7/4/14.
//  Copyright (c) 2014 BookBoi Inc. All rights reserved.
//

#import "EditProfileViewController.h"

@interface EditProfileViewController ()

@property (weak, nonatomic) IBOutlet UITextField *fullName;
@property (weak, nonatomic) IBOutlet UITextField *phoneNumber;
@property (weak, nonatomic) IBOutlet UITextField *collegeMajor;
@property (weak, nonatomic) IBOutlet UITextField *userName;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *pin;
- (IBAction)saveChanges:(id)sender;

@end

@implementation EditProfileViewController

 

NSMutableData* responseData;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setNeedsStatusBarAppearanceUpdate];
    
    [self.phoneNumber setDelegate:self];
    
    self.email = [[NSUserDefaults standardUserDefaults]
                  stringForKey:@"myEmail"];
    
    [self loadProfileData];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.phoneNumber resignFirstResponder];
    [self saveData];
    return YES;
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void)viewDidAppear:(BOOL)animated
{
    self.email = [[NSUserDefaults standardUserDefaults]
                  stringForKey:@"myEmail"];
}

-(void)viewWillAppear:(BOOL)animated
{
    self.email = [[NSUserDefaults standardUserDefaults]
                  stringForKey:@"myEmail"];
}


//Get Data from PHP Http Server Request:
-(void)loadProfileData
{
    
    self.email = [[NSUserDefaults standardUserDefaults]
                  stringForKey:@"myEmail"];
    
    NSString *baseURL = @"http://52.24.55.43/b9ef165b255673dde47bff07f4390fb1/myprofile.php?email=";
    
    baseURL = [baseURL stringByAppendingString:self.email];
    
    NSURL *url = [NSURL URLWithString:baseURL];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLResponse *response;
    NSError *error;
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    NSArray *skimps = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableContainers error:nil];
    

        
    self.userName.text =  skimps[0][@"username"];
    self.fullName.text = skimps[0][@"name"];
    self.collegeMajor.text = skimps[0][@"major"];
    self.phoneNumber.text = skimps[0][@"phone"];
    
}



-(void)saveData
{
    
    NSString *baseURL = @"http://52.24.55.43/b9ef165b255673dde47bff07f4390fb1/setuserdata.php?name=";
    NSString *finalURL1 = [baseURL stringByAppendingString:self.fullName.text];
    NSString *finalURL2 = [finalURL1 stringByAppendingString:@"&email="];
    NSString *finalURL3 = [finalURL2 stringByAppendingString:self.email];
    NSString *finalURL4 = [finalURL3 stringByAppendingString:@"&major="];
    NSString *finalURL5 = [finalURL4 stringByAppendingString:self.collegeMajor.text];
    NSString *finalURL6 = [finalURL5 stringByAppendingString:@"&phone="];
    NSString *finalURL7 = [finalURL6 stringByAppendingString:self.phoneNumber.text];
    NSString *finalURL8 = [finalURL7 stringByAppendingString:@"&username="];
    NSString *finalURL9 = [finalURL8 stringByAppendingString:self.userName.text];
    
    //OCD Space Removal:
    finalURL9 = [finalURL9 stringByReplacingOccurrencesOfString:@" "
                                                     withString:@"%20"];
    finalURL9 = [finalURL9 stringByReplacingOccurrencesOfString:@" "
                                                     withString:@"%20"];
    
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:finalURL9]
                                                           cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                                       timeoutInterval:100];
    
    
    NSError *requestError;
    NSURLResponse *urlResponse = nil;
    [NSURLConnection sendSynchronousRequest:request returningResponse:&urlResponse error:&requestError];
    
    UIAlertView * alert =[[UIAlertView alloc ] initWithTitle:@"Success!"
                                                     message:@"Profile Saved."
                                                    delegate:self
                                           cancelButtonTitle:@"OK"
                                           otherButtonTitles: nil];
    [alert show];
    
}

- (IBAction)saveChanges:(id)sender
{

    [self saveData];
    
}
@end
