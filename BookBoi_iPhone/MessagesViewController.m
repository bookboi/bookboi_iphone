
//  MessagesViewController.m
//  InfiniteMessage
//
//  Created by Levi mallik on 26/05/2015.
//  Copyright (c) 2015 BookBoi Inc. All rights reserved.

#import "MessagesViewController.h"

@interface MessagesViewController ()

@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSMutableArray *tableData1;
@property (strong, nonatomic) NSMutableArray *tableData2;
@property (nonatomic, strong) NSTimer *timer;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *textLabel;
@property (weak, nonatomic) IBOutlet UIImageView *pictureBox;
@property (weak, nonatomic) IBOutlet UILabel *nomessage;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
- (IBAction)sendPress:(id)sender;

@end

@implementation MessagesViewController

NSInteger rowNum;
NSString *username = @"username";
NSString *ImageURL = @"image";
NSString *theMessage = @"message";
NSString *thePrice = @"free";
NSString *theUsername = @"user";
NSString *theSkimpID = @"skimpid";
NSString *theTitle = @"title";


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [NSThread detachNewThreadSelector:@selector(loadRepeat) toTarget:self withObject:nil];
    
    self.pictureBox.layer.cornerRadius = 5.0;
    self.pictureBox.layer.masksToBounds = YES;

    [self loadRepeat];
        
    [self setPicture];
    
    username = [[NSUserDefaults standardUserDefaults]
                          stringForKey:@"myUsername"];
    
    [self.scrollView setContentSize:CGSizeMake(self.scrollView.bounds.size.width, self.scrollView.bounds.size.height*2)];
    
    self.textField.delegate = self;
}


-(void)loadRepeat
{
    self.timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(loadMessages) userInfo:nil repeats:YES];
}

-(void)setPicture
{
    ImageURL = [[NSUserDefaults standardUserDefaults]
                          stringForKey:@"imageURL"];
    
    NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:ImageURL]];
    
    self.pictureBox.image = [UIImage imageWithData:imageData];
    
    self.textLabel.text = theMessage;
    
    self.priceLabel.text = thePrice;
    
    self.titleLabel.text = theTitle;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self.timer invalidate];
}

-(void)viewWillAppear:(BOOL)animated
{
    if(theSkimpID.length != 0)
    {
        ImageURL = [[NSUserDefaults standardUserDefaults]
                stringForKey:@"imageURL"];
    
        theMessage = [[NSUserDefaults standardUserDefaults]
                stringForKey:@"theMessage"];
    
        thePrice = [[NSUserDefaults standardUserDefaults]
                  stringForKey:@"thePrice"];
    
        theUsername = [[NSUserDefaults standardUserDefaults]
                  stringForKey:@"theUsername"];
    
        theSkimpID = [[NSUserDefaults standardUserDefaults]
                stringForKey:@"theSkimpID"];
    
        username = [[NSUserDefaults standardUserDefaults]
                stringForKey:@"myUsername"];
    
        theTitle = [[NSUserDefaults standardUserDefaults]
                stringForKey:@"theTitle"];
    
        [self setPicture];
        
    }

    NSLog(@"%@", ImageURL); NSLog(@"%@", theMessage); NSLog(@"%@", thePrice);
    NSLog(@"%@", theUsername);  NSLog(@"%@", theSkimpID);  NSLog(@"%@", username);
    NSLog(@"%lu", (unsigned long)theSkimpID.length);
    
    
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}


-(void)loadMessages
{
    self.tableData1 = [[NSMutableArray alloc] init];
    self.tableData2 = [[NSMutableArray alloc] init];
    
    
    username = [[NSUserDefaults standardUserDefaults]
                stringForKey:@"myUsername"];
    
    NSString *convid = [[NSUserDefaults standardUserDefaults]
                stringForKey:@"convID"];
    
    
    NSString *url = @"http://52.24.55.43/getmessage.php?username=";
    
    if(theUsername.length==0)
        theUsername = @"dummy";
    
    if(theSkimpID.length==0)
        theSkimpID = @"dummy";
    
    if(convid.length==0)
        convid = @"dummy";
    
    url = [url stringByAppendingString:username];
    url = [url stringByAppendingString:@"&getterid="];
    url = [url stringByAppendingString:theUsername];
    url = [url stringByAppendingString:@"&skimpid="];
    url = [url stringByAppendingString:theSkimpID];
    
    for(int i = 0; i < 25; i++)
    {
        
        url = [url stringByReplacingOccurrencesOfString:@" "
                                             withString:@"%20"];
    }
    
    NSURL *theurl = [NSURL URLWithString:url];
    NSURLRequest *request = [NSURLRequest requestWithURL:theurl];
    NSURLResponse *response;
    NSError *error;
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    NSArray *skimps = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableContainers error:nil];
    
    
    for (int i=0; i<skimps.count; i++)
    {
        [self.tableData1 addObject:skimps[i][@"username"]];
    }
    
    if(self.tableData1.count > 0)
    {
        [self.nomessage setHidden:TRUE];
    }
    
    for (int i=0; i<skimps.count; i++)
    {
        [self.tableData2 addObject:skimps[i][@"message"]];
    }
    
    rowNum = [self.tableData1 count];
    
    [self.tableView reloadData];
    
    [self.tableView scrollRectToVisible:CGRectMake(0, self.tableView.contentSize.height - self.tableView.bounds.size.height, self.tableView.bounds.size.width, self.tableView.bounds.size.height) animated:YES];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.textField resignFirstResponder];
    
    self.textField.text = @"Type your message here...";
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self createMessage];
    return YES;
}

-(void)createMessage
{
     NSLog(@"%@", username);
     NSLog(@"%@", theSkimpID);
     NSLog(@"%@", theUsername);
     NSLog(@"%@", self.textField.text);
    
    NSString *url = @"http://52.24.55.43/createmessage.php?username=";
    
    url = [url stringByAppendingString:username];
    url = [url stringByAppendingString:@"&message="];
    url = [url stringByAppendingString:self.textField.text];
    url = [url stringByAppendingString:@"&skimpid="];
    url = [url stringByAppendingString:theSkimpID];
    url = [url stringByAppendingString:@"&getterid="];
    url = [url stringByAppendingString:theUsername];
    
    for(int i = 0; i < 15; i++)
    {
        url = [url stringByReplacingOccurrencesOfString:@" "
                                             withString:@"%20"];
    }
    
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
                                                           cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                                       timeoutInterval:100];
    
    NSError *requestError;
    NSURLResponse *urlResponse = nil;
    
    NSData *response = [NSURLConnection sendSynchronousRequest:request
                                             returningResponse:&urlResponse error:&requestError];
    self.textField.text = @"";

}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 75;
    
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return rowNum;
    
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellIdentifier = @"skimpCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    UILabel *titleLabel = (UILabel *)[cell.contentView viewWithTag:3];
    UILabel *subtitleLabel = (UILabel *)[cell.contentView viewWithTag:1];
    
    NSString *title = [self.tableData1 objectAtIndex:indexPath.row];
    
    [subtitleLabel setText:[self.tableData2 objectAtIndex:indexPath.row]];
    
    [titleLabel setText: title];
    
    return cell;
    
}
- (IBAction)sendPress:(id)sender
{
    [self createMessage];
    
    [self.textField resignFirstResponder];
}
@end
