//
//  AppDelegate.h
//  BookBoi_iPhone
//
//  Created by Anan Mallik on 7/4/14.
//  Copyright (c) 2014 BookBoi Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>


@property (strong, nonatomic) UIWindow *window;



@end

