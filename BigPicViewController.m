//
//  BigPicViewController.m
//  SKIMP
//
//  Created by Levi mallik on 27/05/2015.
//  Copyright (c) 2015 BookBoi Inc. All rights reserved.
//

#import "BigPicViewController.h"

@interface BigPicViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *picBig;

@end

NSString *ImageURL;

@implementation BigPicViewController

- (void)viewDidLoads
{
    [super viewDidLoad];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        
        sleep(0.1);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            ImageURL = [[NSUserDefaults standardUserDefaults]
                        stringForKey:@"imageURL"];
            
            NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:ImageURL]];
            
            self.picBig.image = [UIImage imageWithData:imageData];
            
        });
    });
    

}


-(void)viewWillAppear:(BOOL)animated
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        
        sleep(0.1);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            ImageURL = [[NSUserDefaults standardUserDefaults]
                        stringForKey:@"imageURL"];
            
            NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:ImageURL]];
            
            self.picBig.image = [UIImage imageWithData:imageData];
            
        });
    });
}
@end
