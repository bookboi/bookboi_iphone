//
//  PayPalWebViewController.m
//  SKIMP
//
//  Created by Levi mallik on 01/06/2015.
//  Copyright (c) 2015 BookBoi Inc. All rights reserved.

#import "PayPalWebViewController.h"

@interface PayPalWebViewController ()

@property (weak, nonatomic) IBOutlet UIWebView *viewWeb;
@property (weak, nonatomic) IBOutlet UILabel *headline;

@end




@implementation PayPalWebViewController

UIActivityIndicatorView *spinner;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self spinme];
    
    [self setweb];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)spinme
{
    spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    spinner.frame = CGRectMake(125.00, 250.00, 120.0, 120.0);
    [self.view addSubview:spinner];
    
    [spinner startAnimating];
    
    //Grand Dispatch is Awesome!
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        
        sleep(4);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [spinner stopAnimating];
            
        });
    });
}

-(void)viewWillAppear:(BOOL)animated
{
    [self setweb];
}

-(void)setweb
{
    NSString *myurl = [[NSUserDefaults standardUserDefaults]
                stringForKey:@"myUrl"];
    
    self.headline.text = [[NSUserDefaults standardUserDefaults]
                  stringForKey:@"myPay"];
    
    NSURL *url = [NSURL URLWithString:myurl];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    [_viewWeb loadRequest:requestObj];
}

@end
