//
//  RegistrationViewController.h
//  BookBoi_iPhone
//
//  Created by Anan Mallik on 8/14/14.
//  Copyright (c) 2014 Mallik. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DropDownListView.h"

@interface RegistrationViewController :UIViewController<kDropDownListViewDelegate>
{
    DropDownListView * Dropobj;
}
@end
 