//  CreateSkimp.m
//  SKIMP
//
//  Created by Anan Mallik on 4/4/15.
//  Copyright (c) 2015 BookBoi Inc. All rights reserved.


#import "CreateSkimp.h"
#import "DropDownListView.h"
#import <CoreLocation/CoreLocation.h>


@interface CreateSkimp()

@property (weak, nonatomic) IBOutlet UITextField *messageBox;
@property (weak, nonatomic) IBOutlet UIImageView *picture;
@property (weak, nonatomic) IBOutlet UIView *photoChoiceView;
@property (weak, nonatomic) IBOutlet UITextField *titlebox;
@property (weak, nonatomic) IBOutlet UITextField *price;
@property (weak, nonatomic) IBOutlet UIButton *camera;
@property (weak, nonatomic) IBOutlet UIButton *skimpbutton;

- (IBAction)chooseCamera:(id)sender;
- (IBAction)takeCameraPic:(id)sender;
- (IBAction)pickPhoto:(id)sender;
- (IBAction)postSkimp:(id)sender;

@property (nonatomic, strong) NSArray *arryList;
@property (nonatomic, strong) NSArray *arryList2;
@property (weak, nonatomic) IBOutlet UILabel *lblSelectedCountryNames;
@property (weak, nonatomic) IBOutlet UIButton *dropDown;

- (IBAction)dropDownPress:(id)sender;

@end


@implementation CreateSkimp

NSString *cat;
CLLocationManager *locationManager;
NSString *latitude;
NSString *longitude;
NSString *finalgps;
NSString *deviceid;
NSInteger randomNumber;
bool flag = FALSE;
NSString *email; NSString *phone; NSString *username;

-(void)viewDidLoad
{
    [super viewDidLoad];
    
    email = [[NSUserDefaults standardUserDefaults]
             stringForKey:@"myEmail"];
    
    username = [[NSUserDefaults standardUserDefaults]
                stringForKey:@"myUsername"];
    
    NSLog(@"%@",email); NSLog(@"%@",deviceid);
    
    flag = false;
    
    [self dropShadow];
    
    [self setNeedsStatusBarAppearanceUpdate];
    
    self.messageBox.delegate = self;
    self.price.delegate = self;
    self.titlebox.delegate = self;
    
    self.arryList=@[@"Movies, Books & Games", @"Technology",@"Cars & Autos",@"Housing",@"Sports & Leisure Eqp.", @"Fashion & Accessories",@"Events",@"Misc.",@"Tutoring"];
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate=self;
    locationManager.desiredAccuracy=kCLLocationAccuracyBest;
    locationManager.distanceFilter=kCLDistanceFilterNone;
    [locationManager requestWhenInUseAuthorization];
    [locationManager startMonitoringSignificantLocationChanges];
    [locationManager startUpdatingLocation];
}


-(void)dropShadow
{
    
    self.picture.layer.cornerRadius = 7.0;
    self.picture.layer.shadowColor = [UIColor blackColor].CGColor;
    self.picture.layer.shadowOffset = CGSizeMake(0, 2);
    self.picture.layer.shadowOpacity = 0.5;
    self.picture.layer.shadowRadius = 2.0;
    self.picture.layer.masksToBounds = NO;
    
    self.camera.layer.cornerRadius = 7.0;
    self.camera.layer.shadowColor = [UIColor blackColor].CGColor;
    self.camera.layer.shadowOffset = CGSizeMake(0, 2);
    self.camera.layer.shadowOpacity = 0.5;
    self.camera.layer.shadowRadius = 2.0;
    self.camera.layer.masksToBounds = NO;
    
    self.messageBox.layer.cornerRadius = 7.0;
    self.messageBox.layer.shadowColor = [UIColor blackColor].CGColor;
    self.messageBox.layer.shadowOffset = CGSizeMake(0, 2);
    self.messageBox.layer.shadowOpacity = 0.5;
    self.messageBox.layer.shadowRadius = 2.0;
    self.messageBox.layer.masksToBounds = NO;
    
    self.titlebox.layer.cornerRadius = 7.0;
    self.titlebox.layer.shadowColor = [UIColor blackColor].CGColor;
    self.titlebox.layer.shadowOffset = CGSizeMake(0, 2);
    self.titlebox.layer.shadowOpacity = 0.5;
    self.titlebox.layer.shadowRadius = 2.0;
    self.titlebox.layer.masksToBounds = NO;
    
    self.price.layer.cornerRadius = 7.0;
    self.price.layer.shadowColor = [UIColor blackColor].CGColor;
    self.price.layer.shadowOffset = CGSizeMake(0, 2);
    self.price.layer.shadowOpacity = 0.5;
    self.price.layer.shadowRadius = 2.0;
    self.price.layer.masksToBounds = NO;
    
    self.dropDown.layer.cornerRadius = 7.0;
    self.dropDown.layer.shadowColor = [UIColor blackColor].CGColor;
    self.dropDown.layer.shadowOffset = CGSizeMake(0, 2);
    self.dropDown.layer.shadowOpacity = 0.5;
    self.dropDown.layer.shadowRadius = 2.0;
    self.dropDown.layer.masksToBounds = NO;
    
    self.skimpbutton.layer.cornerRadius = 7.0;
    self.skimpbutton.layer.shadowColor = [UIColor blackColor].CGColor;
    self.skimpbutton.layer.shadowOffset = CGSizeMake(0, 2);
    self.skimpbutton.layer.shadowOpacity = 0.5;
    self.skimpbutton.layer.shadowRadius = 2.0;
    self.skimpbutton.layer.masksToBounds = NO;
    
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    
    if([text isEqualToString:@"\n"])
    {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}


#pragma mark - CLLocationManagerDelegate
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorAlert show];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{

    CLLocation *currentLocation = newLocation;
    
    
    if (currentLocation != nil)
    {
        longitude = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude];
        latitude = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude];
    }
    
}

- (IBAction)chooseCamera:(id)sender
{
    
    [self.messageBox resignFirstResponder];
    
    [self openTray];

}

- (IBAction)takeCameraPic:(id)sender
{
    
    [self takePicture];
    
}


-(void)openTray
{
    
    [UIView animateWithDuration:0.5
                     animations:^{
                         [self.photoChoiceView setFrame:CGRectMake(0,460,self.photoChoiceView.frame.size.width, self.photoChoiceView.frame.size.height)];
                     }];
    
    flag = TRUE;
}


-(void)closeTray
{
    [UIView animateWithDuration:0.5
                     animations:^{
                         [self.photoChoiceView setFrame:CGRectMake(0,800,self.photoChoiceView.frame.size.width, self.photoChoiceView.frame.size.height)];
                     }];
}

- (IBAction)pickPhoto:(id)sender
{
    [self attachFile];
}

- (IBAction)postSkimp:(id)sender
{
    [self upLoad];
    
    [self skimpIt];
    
    [self updateSkimpCredit];
}

-(void)attachFile
{
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:picker animated:YES completion:NULL];
    
    flag = TRUE;
}

- (void) textFieldDidBeginEditing:(UITextField *) textField
{
    if([self.titlebox.text isEqualToString:@" Title"])
    {
        self.titlebox.text = @"";
    }
    
    if([self.price.text isEqualToString:@"$0.00"])
        self.price.text = @"$";
    
    if([self.messageBox.text isEqualToString:@" Description..."])
        self.messageBox.text = @"";
}


-(void)takePicture
{
    // Create image picker controller
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    
    // Set source to the camera
    imagePicker.sourceType =  UIImagePickerControllerSourceTypeCamera;
    
    // Delegate is self
    imagePicker.delegate = self;
    
    // Allow editing of image ?
    imagePicker.allowsImageEditing = YES;
    
    // Show image picker
    [self presentModalViewController:imagePicker animated:YES];

}


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    UIImage *image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
    
    // Save image
    UIImageWriteToSavedPhotosAlbum(image, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
    
    UIImage *selectedImage = info[UIImagePickerControllerEditedImage];
    self.picture.image = selectedImage;
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}


- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    UIAlertView *alert;

    
    // Unable to save the image
    if (error)
        alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                           message:@"Unable to save image to Photo Album."
                                          delegate:self cancelButtonTitle:@"Ok"
                                 otherButtonTitles:nil];
    else // All is well
        alert = [[UIAlertView alloc] initWithTitle:@"Success"
                                           message:@"Image saved to Photo Album."
                                          delegate:self cancelButtonTitle:@"Ok"
                                 otherButtonTitles:nil];
}

-(void)upLoad
{
    
        randomNumber = arc4random() % 9999999999;
    
        NSLog(@"%ld", (long)randomNumber);
    
        NSString *random = [NSString stringWithFormat:@"%ld", (long)randomNumber];

        NSData *imageData = UIImageJPEGRepresentation(self.picture.image, 0.2);
        NSString *urlString = @"http://52.24.55.43/b9ef165b255673dde47bff07f4390fb1/upload.php";
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setURL:[NSURL URLWithString:urlString]];
        [request setHTTPMethod:@"POST"];
        
        NSString *boundary = @"---------------------------14737809831466499882746641449"
        ;
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
        [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
        
        NSMutableData *body = [NSMutableData data];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
        NSString *filename = @"Content-Disposition: form-data; name=\"userfile\"; filename=\"";
    
        filename  = [filename stringByAppendingString:random];
    
        filename  = [filename stringByAppendingString:@".jpg\"\r\n"];
    
        [body appendData:[filename dataUsingEncoding:NSUTF8StringEncoding]];
    
        [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[NSData dataWithData:imageData]];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [request setHTTPBody:body];
        
        NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
        NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
        NSLog(@"heyo");

}

- (IBAction)dropDownPress:(id)sender
{
    [self.messageBox resignFirstResponder];
    
    [Dropobj fadeOut];
    [self showPopUpWithTitle:@"Select Category" withOption:self.arryList xy:CGPointMake(16, 80) size:CGSizeMake(287, 445) isMultiple:NO];
    
}

-(BOOL)textViewShouldReturn:(UITextView *)textField
{
    [textField resignFirstResponder];
    [self skimpIt];
    return YES;
}

-(void)skimpIt
{
    if (self.messageBox.text && self.messageBox.text.length == 0)
    {
        UIAlertView * alert =[[UIAlertView alloc ] initWithTitle:@"Oops..."
                                                         message:@"Please Enter a message to create a Skimp."
                                                        delegate:self
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles: nil];
        [alert show];
    }
    else if (cat.length == 0)
    {
        UIAlertView * alert =[[UIAlertView alloc ] initWithTitle:@"Oops..."
                                                         message:@"Select a category to create a Skimp."
                                                        delegate:self
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles: nil];
        [alert show];
    }
    else if ([self.titlebox.text isEqualToString:@" Title"])
    {
        UIAlertView * alert =[[UIAlertView alloc ] initWithTitle:@"Oops..."
                                                         message:@"Please Enter a Title."
                                                        delegate:self
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles: nil];
        [alert show];
    }
    else if ([self.messageBox.text isEqualToString:@"Description..."])
    {
        UIAlertView * alert =[[UIAlertView alloc ] initWithTitle:@"Oops..."
                                                         message:@"Please Enter a Message."
                                                        delegate:self
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles: nil];
        [alert show];
    }

    else if ([self.price.text isEqualToString:@"$0.00"])
    {
        UIAlertView * alert =[[UIAlertView alloc ] initWithTitle:@"Oops..."
                                                         message:@"Please Enter a Price. Unless it's Free."
                                                        delegate:self
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles: nil];
        [alert show];
        
        self.price.text = @"Free";
    }
    else
    {
        
   latitude = [[NSUserDefaults standardUserDefaults]
                 stringForKey:@"myLat"];
   longitude = [[NSUserDefaults standardUserDefaults]
                  stringForKey:@"myLong"];
        
    NSString *gps;
    
    finalgps = [latitude stringByAppendingString:@","];
    finalgps = [finalgps stringByAppendingString:longitude];
    
    gps = finalgps;

    
    NSString *url = @"http://52.24.55.43/b9ef165b255673dde47bff07f4390fb1/createskimp.php?email=";
    
    url = [url stringByAppendingString:email];
    url = [url stringByAppendingString:@"&message="];
    url = [url stringByAppendingString:self.messageBox.text];
    url = [url stringByAppendingString:@"&price="];
    url = [url stringByAppendingString:self.price.text];
    url = [url stringByAppendingString:@"&category="];
    url = [url stringByAppendingString:cat];
    url = [url stringByAppendingString:@"&gps="];
    url = [url stringByAppendingString:gps];
    url = [url stringByAppendingString:@"&username="];
    url = [url stringByAppendingString:username];
    url = [url stringByAppendingString:@"&title="];
    url = [url stringByAppendingString:self.titlebox.text];
    url = [url stringByAppendingString:@"&lat="];
    url = [url stringByAppendingString:latitude];
    url = [url stringByAppendingString:@"&long="];
    url = [url stringByAppendingString:longitude];
    //url = [url stringByAppendingString:@"&deviceid="];
    //url = [url stringByAppendingString:deviceid];
        
    NSString *random = [NSString stringWithFormat:@"%ld", (long)randomNumber];
        
    if(flag)
    {
        url = [url stringByAppendingString:@"&imageid="];
        url = [url stringByAppendingString:random];
        url = [url stringByAppendingString:@".jpg"];
    }
    else
    {
        url = [url stringByAppendingString:@"&imageid=logo.jpg"];
    }
        
        
        for(int i =0; i < 20; i++)
        {
            url = [url stringByReplacingOccurrencesOfString:@" "
                                                 withString:@"%20"];
        }

    
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
                                                           cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                                       timeoutInterval:100];
    
    NSError *requestError;
    NSURLResponse *urlResponse = nil;
    
    NSData *response = [NSURLConnection sendSynchronousRequest:request
                                             returningResponse:&urlResponse error:&requestError];

    NSLog(@"%@",response);
    
    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Success!" message:@"Your Skimp is live!" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    [alert show];
        
        
    [self performSegueWithIdentifier:@"returnmyskimp" sender:self];
    }
}

-(void)updateSkimpCredit
{
    
    NSString *email = [[NSUserDefaults standardUserDefaults]
                  stringForKey:@"myEmail"];
    
    NSString *skimpcredit = [[NSUserDefaults standardUserDefaults]
                stringForKey:@"mySkimpCredit"];
    
    
    if(skimpcredit.length==0)
        skimpcredit = @"50";
    

    NSString *baseURL = @"http://52.24.55.43/b9ef165b255673dde47bff07f4390fb1/updateskimpcredit.php?email=";
    
    NSString *finalURL1 = [baseURL stringByAppendingString:email];
    
    finalURL1 = [finalURL1 stringByAppendingString:@"&skimpcredit="];
    
    finalURL1 = [finalURL1 stringByAppendingString:skimpcredit];

    finalURL1 = [finalURL1 stringByReplacingOccurrencesOfString:@" "
                                                     withString:@"%20"];
    finalURL1 = [finalURL1 stringByReplacingOccurrencesOfString:@" "
                                                     withString:@"%20"];
    finalURL1 = [finalURL1 stringByReplacingOccurrencesOfString:@" "
                                                     withString:@"%20"];
    finalURL1 = [finalURL1 stringByReplacingOccurrencesOfString:@" "
                                                     withString:@"%20"];
    
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:finalURL1]
                                                           cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                                       timeoutInterval:100];
    
    
    NSError *requestError;
    NSURLResponse *urlResponse = nil;
    [NSURLConnection sendSynchronousRequest:request returningResponse:&urlResponse error:&requestError];

    
}

-(void)showPopUpWithTitle:(NSString*)popupTitle withOption:(NSArray*)arrOptions xy:(CGPoint)point size:(CGSize)size isMultiple:(BOOL)isMultiple
{
    
    Dropobj = [[DropDownListView alloc] initWithTitle:popupTitle options:arrOptions xy:point size:size isMultiple:isMultiple];
    Dropobj.delegate = self;
    [Dropobj showInView:self.view animated:YES];
    
    /*----------------Set DropDown backGroundColor-----------------*/
    [Dropobj SetBackGroundDropDwon_R:0.0 G:108.0 B:194.0 alpha:0.70];
    
}

- (void)DropDownListView:(DropDownListView *)dropdownListView didSelectedIndex:(NSInteger)anIndex
{
    /*----------------Get Selected Value[Single selection]-----------------*/
    self.dropDown.titleLabel.text=[self.arryList objectAtIndex:anIndex];
    //self.campus = [self.arryList objectAtIndex:anIndex];

    if([[self.arryList objectAtIndex:anIndex] isEqualToString:@"Movies, Books & Games"])
    {
        cat = @"Movies, Books & Games";
    }
    if([[self.arryList objectAtIndex:anIndex] isEqualToString:@"Cars & Autos"])
    {
       cat = @"Cars & Autos";
    }
    if([[self.arryList objectAtIndex:anIndex] isEqualToString:@"Sports & Leisure Eqp."])
    {
       cat = @"Sports & Leisure Eqp.";
    }
    if([[self.arryList objectAtIndex:anIndex] isEqualToString:@"Technology"])
    {
        cat = @"Technology";
    }
    if([[self.arryList objectAtIndex:anIndex] isEqualToString:@"Misc."])
    {
        cat = @"Misc.";
    }
    if([[self.arryList objectAtIndex:anIndex] isEqualToString:@"Events"])
    {
        cat = @"Events";
    }
    if([[self.arryList objectAtIndex:anIndex] isEqualToString:@"Housing"])
    {
        cat = @"Housing";
    }
    if([[self.arryList objectAtIndex:anIndex] isEqualToString:@"Fashion & Accessories"])
    {
        cat = @"Fashion & Accessories";
    }
    if([[self.arryList objectAtIndex:anIndex] isEqualToString:@"Tutoring"])
    {
        cat = @"Tutoring";
    }
    
}
- (void)DropDownListView:(DropDownListView *)dropdownListView Datalist:(NSMutableArray*)ArryData
{
    
    /*----------------Get Selected Value[Multiple selection]-----------------*/
    if (ArryData.count>0)
    {
        _lblSelectedCountryNames.text=[ArryData componentsJoinedByString:@"\n"];
        CGSize size=[self GetHeightDyanamic:_lblSelectedCountryNames];
        _lblSelectedCountryNames.frame=CGRectMake(16, 440, 287, 500);
    }
    else
    {
        _lblSelectedCountryNames.text=@"";
    }
    
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    
    if ([touch.view isKindOfClass:[UIView class]])
    {
        [Dropobj fadeOut];
    }
}


-(CGSize)GetHeightDyanamic:(UILabel*)lbl
{
    NSRange range = NSMakeRange(0, [lbl.text length]);
    CGSize constraint;
    constraint= CGSizeMake(288 ,MAXFLOAT);
    CGSize size;
    
    if (([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0))
    {
        NSDictionary *attributes = [lbl.attributedText attributesAtIndex:0 effectiveRange:&range];
        CGSize boundingBox = [lbl.text boundingRectWithSize:constraint options: NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil].size;
        
        size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
    }
    else
    {
        
        size = [lbl.text sizeWithFont:[UIFont fontWithName:@"Helvetica" size:14] constrainedToSize:constraint lineBreakMode:NSLineBreakByWordWrapping];
        
    }
    return size;
}

@end
