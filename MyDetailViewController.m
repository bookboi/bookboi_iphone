//
//  DetailViewController.m
//  SKIMP
//
//  Created by Levi mallik on 28/05/2015.
//  Copyright (c) 2015 BookBoi Inc. All rights reserved.
//

#import "MyDetailViewController.h"

@interface MyDetailViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *pictureBox;
@property (weak, nonatomic) IBOutlet UILabel *textLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end

@implementation MyDetailViewController

NSString *username;
NSString *ImageURL;
NSString *theMessage;
NSString *thePrice;
NSString *theUsername;
NSString *theSkimpID;
NSString *theTitle;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    ImageURL = [[NSUserDefaults standardUserDefaults]
                stringForKey:@"imageURL"];
    
    theMessage = [[NSUserDefaults standardUserDefaults]
                  stringForKey:@"theMessage"];
    
    thePrice = [[NSUserDefaults standardUserDefaults]
                stringForKey:@"thePrice"];
    
    theUsername = [[NSUserDefaults standardUserDefaults]
                   stringForKey:@"theUsername"];
    
    theSkimpID = [[NSUserDefaults standardUserDefaults]
                  stringForKey:@"theSkimpID"];
    
    username = [[NSUserDefaults standardUserDefaults]
                stringForKey:@"myUsername"];
    
    theTitle = [[NSUserDefaults standardUserDefaults]
                stringForKey:@"theTitle"];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        
        sleep(0.1);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self setPicture];
            
        });
    });
    
    [self setNeedsStatusBarAppearanceUpdate];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}


-(void)viewWillAppear:(BOOL)animated
{
    self.pictureBox.image = [UIImage imageNamed:@"logo.png"];
    
    ImageURL = [[NSUserDefaults standardUserDefaults]
                stringForKey:@"imageURL"];
    
    theMessage = [[NSUserDefaults standardUserDefaults]
                  stringForKey:@"theMessage"];
    
    thePrice = [[NSUserDefaults standardUserDefaults]
                stringForKey:@"thePrice"];
    
    theUsername = [[NSUserDefaults standardUserDefaults]
                   stringForKey:@"theUsername"];
    
    theSkimpID = [[NSUserDefaults standardUserDefaults]
                  stringForKey:@"theSkimpID"];
    
    username = [[NSUserDefaults standardUserDefaults]
                stringForKey:@"myUsername"];
    
    theTitle = [[NSUserDefaults standardUserDefaults]
                stringForKey:@"theTitle"];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        
        sleep(0.1);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self setPicture];
            
        });
    });
    
}

-(void)setPicture
{
    ImageURL = [[NSUserDefaults standardUserDefaults]
                stringForKey:@"imageURL"];
    
    NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:ImageURL]];
    
    self.pictureBox.image = [UIImage imageWithData:imageData];
    
    self.textLabel.text = theMessage;
    
    self.priceLabel.text = thePrice;
    
    self.titleLabel.text = theTitle;
}

@end

