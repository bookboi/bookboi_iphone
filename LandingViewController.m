//
//  LandingViewController.m
//  BookBoi
//
//  Created by Anan Mallik on 1/8/15.
//  Copyright (c) 2015 BookBoi Inc. All rights reserved.

#import "LandingViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface LandingViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *blurryBG;
@property (weak, nonatomic) IBOutlet UIImageView *landingImage;

@end

@implementation LandingViewController

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    
}


-(void)viewDidAppear:(BOOL)animated
{
    
    bool login = [[NSUserDefaults standardUserDefaults]
                  boolForKey:@"myLogin"];
  
    if(login)
    {
        [self performSegueWithIdentifier:@"enterHome" sender:self];
    }
    else
    {
        [self.view setHidden:NO];
    }
    
    
    [super viewDidAppear:animated];
}
    



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




@end
