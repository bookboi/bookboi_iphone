//
//  RegistrationViewController.m
//  BookBoi_iPhone
//
//  Created by Anan Mallik on 8/14/14.
//  Copyright (c) 2014 BookBoi Inc. All rights reserved.
//

#import "RegistrationViewController.h"
#import "DropDownListView.h"
#import "LoginViewController.h"

@interface RegistrationViewController ()

- (IBAction)createUser:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *phone;
@property (weak, nonatomic) IBOutlet UITextField *email;
@property (weak, nonatomic) IBOutlet UILabel *lblSelectedCountryNames;
@property (nonatomic, strong) NSString *myemail;
@property (weak, nonatomic) IBOutlet UITextField *username;

@end

@implementation RegistrationViewController

NSString *serverEmail =@"NO";

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.phone setDelegate:self];

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.phone resignFirstResponder];
    [self userCreation];
    return YES;
}

-(void)userCreation
{

    if([serverEmail isEqualToString:@"YES"])
    {
        UIAlertView * alert =[[UIAlertView alloc ] initWithTitle:@"Oops..."
                                                         message:@"That email has been Used."
                                                        delegate:self
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles: nil];
        [alert show];
        
        [self viewDidLoad];
    }
    else if (self.email.text && self.email.text.length == 0)
    {
        UIAlertView * alert =[[UIAlertView alloc ] initWithTitle:@"Oops..."
                                                         message:@"Enter an email address."
                                                        delegate:self
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles: nil];
        [alert show];
        
        [self viewDidLoad];
    }
    else if (self.username.text && self.username.text.length == 0)
    {
        UIAlertView * alert =[[UIAlertView alloc ] initWithTitle:@"Oops..."
                                                         message:@"Enter a username."
                                                        delegate:self
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles: nil];
        [alert show];
        
        [self viewDidLoad];
    }
    else if ([self.email.text rangeOfString:@"@"].location == NSNotFound)
    {
        UIAlertView * alert =[[UIAlertView alloc ] initWithTitle:@"Oops..."
                                                         message:@"Please Enter a valid email address."
                                                        delegate:self
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles: nil];
        [alert show];
        
        [self viewDidLoad];
    }
    else if ([self.email.text rangeOfString:@"."].location == NSNotFound)
    {
        UIAlertView * alert =[[UIAlertView alloc ] initWithTitle:@"Oops..."
                                                         message:@"Please Enter a valid email address."
                                                        delegate:self
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles: nil];
        [alert show];
        
        [self viewDidLoad];
    }
    else if ([self.email.text rangeOfString:@".com"].location != NSNotFound||[self.email.text rangeOfString:@".edu"].location != NSNotFound||[self.email.text rangeOfString:@".org"].location != NSNotFound||[self.email.text rangeOfString:@".net"].location != NSNotFound||[self.email.text rangeOfString:@".us"].location != NSNotFound)
    {
        NSString *server1 = @"http://52.24.55.43/b9ef165b255673dde47bff07f4390fb1/createuser.php?username=";
        
        NSString *finalURL = [server1 stringByAppendingString:self.username.text];
        
        finalURL = [finalURL stringByAppendingString:@"&email="];
        
        finalURL = [finalURL stringByAppendingString:self.email.text];
        
        finalURL = [finalURL stringByAppendingString:@"&phone="];
        
        finalURL = [finalURL stringByAppendingString:@"5552842056"];
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        
        [defaults setObject:self.email.text forKey:@"myEmail"];
        [defaults setObject:self.phone.text forKey:@"myPhone"];
        [defaults setObject:self.username.text forKey:@"myUsername"];
        
        //OCD Space Removal:
        finalURL = [finalURL stringByReplacingOccurrencesOfString:@" "
                                                       withString:@"%20"];
        finalURL = [finalURL stringByReplacingOccurrencesOfString:@" "
                                                       withString:@"%20"];
        finalURL = [finalURL stringByReplacingOccurrencesOfString:@" "
                                                       withString:@"%20"];
        finalURL = [finalURL stringByReplacingOccurrencesOfString:@" "
                                                       withString:@"%20"];
        finalURL = [finalURL stringByReplacingOccurrencesOfString:@" "
                                                       withString:@"%20"];
        finalURL = [finalURL stringByReplacingOccurrencesOfString:@" "
                                                       withString:@"%20"];
        finalURL = [finalURL stringByReplacingOccurrencesOfString:@" "
                                                       withString:@"%20"];
        finalURL = [finalURL stringByReplacingOccurrencesOfString:@" "
                                                       withString:@"%20"];
        
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:finalURL]
                                                               cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
                                                           timeoutInterval:100];
        
        NSError *requestError;
        NSURLResponse *urlResponse = nil;
        NSData *response = [NSURLConnection sendSynchronousRequest:request returningResponse:&urlResponse error:&requestError];
        
        [self.view endEditing:YES];
        
        
        
        UIAlertView * alert =[[UIAlertView alloc ] initWithTitle:@"Success!"
                                                         message:@"Your 4-Digit PIN was emailed to you! Please Check Spam folder."
                                                        delegate:self
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles: nil];
        [alert show];
        
        [self performSegueWithIdentifier:@"goLogin" sender:self];
        
        [self sendPin];
    }
    
}

- (IBAction)createUser:(id)sender
{
    
    [self userCreation];
    
}

-(void) sendPin
{
    self.myemail = self.email.text;
    
    NSString *baseURL = @"http://52.24.55.43/sendpin.php?email=";
    
    baseURL = [baseURL stringByAppendingString:self.myemail];
    
    NSURL *url = [NSURL URLWithString:baseURL];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    (void)[[NSURLConnection alloc] initWithRequest:request delegate:self];
}


@end
