//
//  PayPalViewController.m
//  SKIMP
//
//  Created by Levi mallik on 31/05/2015.
//  Copyright (c) 2015 BookBoi Inc. All rights reserved.
//

#import "PayPalViewController.h"

@interface PayPalViewController ()

@property (weak, nonatomic) IBOutlet UIButton *venmo;
@property (weak, nonatomic) IBOutlet UIButton *snapcash;
@property (weak, nonatomic) IBOutlet UIButton *paypal;


- (IBAction)goPaypal:(id)sender;
- (IBAction)goSnapcash:(id)sender;
- (IBAction)goVenmo:(id)sender;


@end

@implementation PayPalViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setNeedsStatusBarAppearanceUpdate];

    self.venmo.layer.cornerRadius = 7.0;
    self.venmo.layer.shadowColor = [UIColor blackColor].CGColor;
    self.venmo.layer.shadowOffset = CGSizeMake(0, 2);
    self.venmo.layer.shadowOpacity = 0.5;
    self.venmo.layer.shadowRadius = 2.0;
    self.venmo.layer.masksToBounds = NO;
    
    self.snapcash.layer.cornerRadius = 7.0;
    self.snapcash.layer.shadowColor = [UIColor blackColor].CGColor;
    self.snapcash.layer.shadowOffset = CGSizeMake(0, 2);
    self.snapcash.layer.shadowOpacity = 0.5;
    self.snapcash.layer.shadowRadius = 2.0;
    self.snapcash.layer.masksToBounds = NO;
    
    self.paypal.layer.cornerRadius = 7.0;
    self.paypal.layer.shadowColor = [UIColor blackColor].CGColor;
    self.paypal.layer.shadowOffset = CGSizeMake(0, 2);
    self.paypal.layer.shadowOpacity = 0.5;
    self.paypal.layer.shadowRadius = 2.0;
    self.paypal.layer.masksToBounds = NO;
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}


- (IBAction)goPaypal:(id)sender
{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    [defaults setObject:@"Send $0.99 to billing@paypal.com\n for 50 Credits"forKey:@"myPay"];
    
    [defaults setObject:@"https://www.paypal.com/signin/"forKey:@"myUrl"];
    
    [self performSegueWithIdentifier:@"gopay" sender:self];
    
}

- (IBAction)goSnapcash:(id)sender
{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    [defaults setObject:@"Send $0.99 to skimpapp\n for 50 Credits"forKey:@"myPay"];
    
    [defaults setObject:@"https://accounts.snapchat.com/accounts/login?continue=https%3A%2F%2Faccounts.snapchat.com%2Faccounts%2Fwelcome"forKey:@"myUrl"];
    
    [self performSegueWithIdentifier:@"gopay" sender:self];
    
}

- (IBAction)goVenmo:(id)sender
{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    [defaults setObject:@"Send $0.99 to skimpapp\n for 50 Credits"forKey:@"myPay"];
    
    [defaults setObject:@"https://venmo.com/account/sign-in/"forKey:@"myUrl"];
    
    [self performSegueWithIdentifier:@"gopay" sender:self];
    
}
@end
