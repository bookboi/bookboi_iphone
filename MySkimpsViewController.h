//
//  MySkimpsViewController.h
//  SKIMP
//
//  Created by Anan Mallik on 4/5/15.
//  Copyright (c) 2015 BookBoi Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MySkimpsViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@end
