//
//  MySkimpsViewController.m
//  SKIMP
//
//  Created by Anan Mallik on 4/5/15.
//  Copyright (c) 2015 BookBoi Inc. All rights reserved.
//

#import "MySkimpsViewController.h"
#import <CoreLocation/CoreLocation.h>

@interface MySkimpsViewController()

@property (weak, nonatomic) IBOutlet UILabel *userName;
@property (weak, nonatomic) IBOutlet UILabel *fullName;
@property (weak, nonatomic) IBOutlet UILabel *phoneNumber;
@property (weak, nonatomic) IBOutlet UILabel *collegeMajor;
@property (weak, nonatomic) IBOutlet UILabel *collegeEmail;
@property (nonatomic, strong) NSString *email;
@property (weak, nonatomic) IBOutlet UILabel *active;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (retain, nonatomic) UIAlertView* alertView;
@property (strong, nonatomic) NSMutableArray *tableData1;
@property (strong, nonatomic) NSMutableArray *tableData2;
@property (strong, nonatomic) NSMutableArray *tableData3;
@property (strong, nonatomic) NSMutableArray *tableData4;
@property (strong, nonatomic) NSMutableArray *tableData5;
@property (strong, nonatomic) NSMutableArray *tableData7;
@property (strong, nonatomic) NSMutableArray *tableData8;
@property (weak, nonatomic) IBOutlet UIView *profileView;
@property (weak, nonatomic) IBOutlet UIButton *closeButton;
@property (weak, nonatomic) IBOutlet UILabel *skimpCredit;
- (IBAction)showCredit:(id)sender;

- (IBAction)openProfile:(id)sender;
- (IBAction)closeProfile:(id)sender;
- (IBAction)logOut:(id)sender;

@end

@implementation MySkimpsViewController

NSInteger rowNum;
NSString *myLat;
NSString *myLong;
CLLocationManager *locationManager;
NSInteger tableIndex;
NSString *ImageURL;
NSString *theMessage;
NSString *thePrice;
NSString *theUsername;
NSString *theSkimpID;
NSString *theTitle;
NSArray *skimps;
UIActivityIndicatorView *spinner;

-(void)viewDidLoad
{
    
    [super viewDidLoad];
    
    [self closeTray];
    
    if(self.tableData1.count != 0)
    {
        [self.active setHidden:YES];
    }
    
    [self setNeedsStatusBarAppearanceUpdate];
    
    self.tableView.allowsMultipleSelectionDuringEditing = NO;
    
    [self loadProfileData];
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex

{
    if (buttonIndex == 0)
    {
        //Do Nothing
    }
    else if (buttonIndex == 1)
    {
        [self logmeout];
    }
}


-(void)setPicture
{
    ImageURL = @"http://52.24.55.43/b9ef165b255673dde47bff07f4390fb1/pictures/";
    
    ImageURL = [ImageURL stringByAppendingString:[self.tableData5 objectAtIndex:tableIndex]];
    
    theMessage = skimps[tableIndex][@"message"];
    
    thePrice = skimps[tableIndex][@"price"];
    
    theTitle = skimps[tableIndex][@"title"];
    
    theSkimpID = skimps[tableIndex][@"id"];
}

-(void)viewWillDisappear:(BOOL)animated
{
    
        if(self.tableData1.count != 0)
        {
            [self setPicture];
    
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
            [defaults setObject:ImageURL forKey:@"imageURL"];
    
            [defaults setObject:theMessage forKey:@"theMessage"];
    
            [defaults setObject:thePrice forKey:@"thePrice"];
    
            [defaults setObject:theSkimpID forKey:@"theSkimpID"];
    
            [defaults setObject:theTitle forKey:@"theTitle"];
    
        }

}


-(void)spinme
{
    spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    spinner.frame = CGRectMake(100.00, 250.00, 120.0, 120.0);
    
    spinner.center = self.view.center;
    
    [self.view addSubview:spinner];
    
    [spinner startAnimating];
    
    //Grand Dispatch is Awesome!
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        
        sleep(4);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [spinner stopAnimating];
            
        });
    });
}

- (void)loadProfileData
{
    self.email = [[NSUserDefaults standardUserDefaults]
                  stringForKey:@"myEmail"];
    
    NSString *baseURL = @"http://52.24.55.43/b9ef165b255673dde47bff07f4390fb1/myprofile.php?email=";
    
    baseURL = [baseURL stringByAppendingString:self.email];
    
    
    for(int i = 0; i < 20; i++)
    {
        
        baseURL = [baseURL stringByReplacingOccurrencesOfString:@" "
                                                 withString:@"%20"];
    }
    
    NSURL *url = [NSURL URLWithString:baseURL];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLResponse *response;
    NSError *error;
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    NSArray *skimps = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableContainers error:nil];
    
    self.userName.text =  skimps[0][@"username"];
    self.fullName.text = skimps[0][@"name"];
    self.collegeMajor.text = skimps[0][@"major"];
    self.phoneNumber.text = skimps[0][@"phone"];
    self.skimpCredit.text = skimps[0][@"skimpcredit"];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    [defaults setObject:skimps[0][@"phone"] forKey:@"myPhone"];
    [defaults setObject:skimps[0][@"username"] forKey:@"myUsername"];
    [defaults setObject:skimps[0][@"skimpcredit"] forKey:@"mySkimpCredit"];
    
    self.collegeEmail.text = self.email;
    
}

-(void)viewWillAppear:(BOOL)animated
{
    
    [self closeTray];
    
    [self.active setHidden:NO];
    
    [self populatemyData];
    
    [self loadProfileData];
    
    if(self.tableData1.count != 0)
    {
        
        [self.active setHidden:YES];
        
        [self setPicture];
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        
        [defaults setObject:ImageURL forKey:@"imageURL"];
        
        [defaults setObject:theMessage forKey:@"theMessage"];
        
        [defaults setObject:thePrice forKey:@"thePrice"];
        
        [defaults setObject:theSkimpID forKey:@"theSkimpID"];
        
        [defaults setObject:theTitle forKey:@"theTitle"];
        
    }

    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate=self;
    locationManager.desiredAccuracy=kCLLocationAccuracyBest;
    locationManager.distanceFilter=kCLDistanceFilterNone;
    [locationManager requestWhenInUseAuthorization];
    [locationManager startMonitoringSignificantLocationChanges];
    [locationManager startUpdatingLocation];
    
}

#pragma mark - CLLocationManagerDelegate
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorAlert show];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    
    CLLocation *currentLocation = newLocation;
    
    
    myLong = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude];
    myLat= [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude];

}


-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}


-(void)endtheSkimp
{
    
    NSString *baseURL = @"http://52.24.55.43/b9ef165b255673dde47bff07f4390fb1/removeskimp.php?id=";
    
    NSString *result = [NSString stringWithFormat:@"%@", [self.tableData4 objectAtIndex:tableIndex]];
    
    baseURL = [baseURL stringByAppendingString:result];
    
    NSURL *url = [NSURL URLWithString:baseURL];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    [self populatemyData];
    
    [self.tableView reloadData];
    
    [self populatemyData];
    
    [self.tableView reloadData];

}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    tableIndex = indexPath.row;
    
    [self performSegueWithIdentifier:@"goedit" sender:self];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    tableIndex = indexPath.row;
    
    return YES;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    tableIndex = indexPath.row;
    
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        [self endtheSkimp];
        
        for(int i = 0; i < 4; i++)
        {
            [self.tableView reloadData];
            [self viewDidLoad];
        }
        
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Success!" message:@"Your Skimp was Deleted." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        
        [alert show];
    }
    
}
-(void) populatemyData
{
    myLat = [[NSUserDefaults standardUserDefaults]
             stringForKey:@"myLat"];
    myLong = [[NSUserDefaults standardUserDefaults]
              stringForKey:@"myLong"];
    
    NSString *email;
    
    email = [[NSUserDefaults standardUserDefaults]
             stringForKey:@"myEmail"];
    
    
    self.tableData1 = [[NSMutableArray alloc] init];
    self.tableData2 = [[NSMutableArray alloc] init];
    self.tableData3 = [[NSMutableArray alloc] init];
    self.tableData4 = [[NSMutableArray alloc] init];
    self.tableData5 = [[NSMutableArray alloc] init];
    self.tableData7 = [[NSMutableArray alloc] init];
    self.tableData8 = [[NSMutableArray alloc] init];
    
    NSString *baseURL = @"http://52.24.55.43/b9ef165b255673dde47bff07f4390fb1/myskimps.php?email=";
    
    baseURL = [baseURL stringByAppendingString:email];
    
    NSURL *url = [NSURL URLWithString:baseURL];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLResponse *response;
    NSError *error;
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    skimps = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableContainers error:nil];
    
    
    for (int i=0; i<skimps.count; i++)
    {
        [self.tableData1 addObject:skimps[i][@"email"]];
    }
    
    for (int i=0; i<skimps.count; i++)
    {
        [self.tableData5 addObject:skimps[i][@"imageid"]];
    }
    
    for (int i=0; i<skimps.count; i++)
    {
        [self.tableData7 addObject:skimps[i][@"title"]];
    }
    
    for (int i=0; i<skimps.count; i++)
    {
        [self.tableData8 addObject:skimps[i][@"price"]];
    }
    
    for (int i=0; i<skimps.count; i++)
    {
        NSString *result = [NSString stringWithFormat:@"%@", skimps[i][@"id"]];
        
        [self.tableData4 addObject:result];
    }
    for (int i=0; i<skimps.count; i++)
    {
        NSString *gps = skimps[i][@"gps"];
        
        NSArray *split = [gps componentsSeparatedByString: @","];
        
        CLLocation *locA = [[CLLocation alloc] initWithLatitude:(CGFloat)[split[0] floatValue] longitude:(CGFloat)[split[1] floatValue]];
        
        CLLocation *locB = [[CLLocation alloc] initWithLatitude:(CGFloat)[myLat floatValue]  longitude:(CGFloat)[myLong floatValue] ];
        
        CLLocationDistance distance = [locA distanceFromLocation:locB];
        
        distance = distance/1609.34;
        
        [self.tableData3 addObject:[NSString stringWithFormat:@"%0.1f mile", distance]];
    }
    
    for (int i=0; i<skimps.count; i++)
    {
        [self.tableData2 addObject:skimps[i][@"message"]];
    }
    
    
    rowNum = [self.tableData1 count];
    
    if(self.tableData1.count != 0)
    {
        [self.active setHidden:YES];
    }
    
    if(self.tableData1.count == 0)
    {
        [self.active setHidden:NO];
    }
}


-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 100;
    
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return rowNum;
    
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellIdentifier = @"skimpCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    UILabel *titleLabel = (UILabel *)[cell.contentView viewWithTag:3];
    UILabel *subtitleLabel = (UILabel *)[cell.contentView viewWithTag:1];
    UILabel *ageLabel = (UILabel *)[cell.contentView viewWithTag:2];
    
    UIImageView *image = (UIImageView *)[cell.contentView viewWithTag:7];
    
    NSString *title = [self.tableData1 objectAtIndex:indexPath.row];
    [subtitleLabel setText:[self.tableData2 objectAtIndex:indexPath.row]];
    [ageLabel setText:[self.tableData3 objectAtIndex:indexPath.row]];
    
    image.layer.cornerRadius = 7.0;
    image.layer.masksToBounds = YES;
    
    //Cool Image Setter Code---->>>>>>>>>>>>>>
    NSString *ImageURL = @"http://52.24.55.43/b9ef165b255673dde47bff07f4390fb1/pictures/";
    ImageURL = [ImageURL stringByAppendingString:[self.tableData5 objectAtIndex:indexPath.row]];
    
    NSURL *imageURL = [NSURL URLWithString:ImageURL];
    
    image.image = [UIImage imageNamed:@"logo.png"];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        
        sleep(0.2);
        
        NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            UIImage *setImage = [UIImage imageWithData:imageData];
            [image setImage:setImage];
        });
    });
    
    
    [titleLabel setText: title];

    return cell;
    
    [self.tableView reloadData];
}



- (IBAction)showCredit:(id)sender
{
    NSString *credits;
    
    credits = @"You have ";
    
    credits = [credits stringByAppendingString:self.skimpCredit.text];
    
    credits = [credits stringByAppendingString:@" Skimps left!\n\n[Buy a 50 pack for $0.99]"];
    
    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"My SKiMP Credit" message:credits delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    [alert show];
}

- (IBAction)openProfile:(id)sender
{
    
    
    [self.closeButton setHidden:NO];

    [UIView animateWithDuration:0.5
                     animations:^{
                         [self.profileView setFrame:CGRectMake(0,70,self.profileView.frame.size.width, self.profileView.frame.size.height)];
                     }];

    
} 

-(void)closeTray
{
    [self.closeButton setHidden:YES];
    
    [UIView animateWithDuration:0.5
                     animations:^{
                         [self.profileView setFrame:CGRectMake(450,70,self.profileView.frame.size.width, self.profileView.frame.size.height)];
                     }];
}

- (IBAction)closeProfile:(id)sender
{
    [self closeTray];
}


-(void)logmeout
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setBool:FALSE forKey:@"myLogin"];
    
    [self performSegueWithIdentifier:@"logout" sender:self];
}

- (IBAction)logOut:(id)sender
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                    message:@"Logout of SKiMP?"
                                                   delegate:self
                                          cancelButtonTitle:@"Cancel"
                                          otherButtonTitles:@"Yes",nil];
    [alert show];
}


@end
